<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domains', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('short_name')->unique();
            $table->mediumText('address')->nullable();
            $table->string('gstinno')->unique()->nullable();
            $table->mediumText('logo')->nullable();
            $table->string('email')->nullable();
            $table->string('website')->nullable();
            $table->string('mobile')->nullable();
            $table->string('mobile2')->nullable();
            $table->string('landline')->nullable();
            $table->string('bankaccno')->nullable();
            $table->string('bankname')->nullable();
            $table->string('ifsc')->nullable();
            $table->tinyInteger('is_deleted')->default('0');
            $table->integer('deleted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domains');
    }
}
