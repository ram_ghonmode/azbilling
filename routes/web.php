<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// Routes Created By Sankalp On 16 November, 2017
Route::get('/domain', 'HomeController@domain')->name('domain');
Route::post('/domain', 'HomeController@domain')->name('domain.submit');
Route::get('/customer', 'HomeController@customer')->name('customer');
Route::post('/customer', 'HomeController@customer')->name('customer.submit');
Route::get('/taxes', 'HomeController@taxes')->name('taxes');
Route::post('/taxes', 'HomeController@taxes')->name('taxes.submit');
Route::get('/taxgroup', 'HomeController@taxgroup')->name('taxgroup');
Route::get('/edittax/{id}', 'HomeController@edittax')->name('edittax');
Route::post('/edittax/{id}', 'HomeController@edittax')->name('edittax.submit');
Route::get('/edittaxgroup/{id}', 'HomeController@edittaxgroup')->name('edittaxgroup');
Route::post('/edittaxgroup/{id}', 'HomeController@edittaxgroup')->name('edittaxgroup.submit');
Route::post('/taxgroup', 'HomeController@taxgroup')->name('taxgroup.submit');
Route::get('/delete/{id}/{model}', 'HomeController@delete')->name('delete.submit');
Route::get('/editcustomer/{id}','HomeController@editcustomer')->name('editcustomer');
Route::post('/editcustomer/{id}','HomeController@editcustomer')->name('editcustomer.submit');
Route::get('invoice_list', 'HomeController@invoice_list')->name('invoice_list');
Route::get('invoice', 'HomeController@invoice')->name('invoice');
Route::post('saveinvoice', 'InvoiceController@saveinvoice');
Route::get('getinvoice/{id}', 'HomeController@printinvoice')->name('getinvoice');
Route::get('editinvoice/{id}', 'HomeController@editinvoice')->name('editinvoice');
Route::post('updateinvoice/{id}', 'InvoiceController@updateinvoice')->name('updateinvoice');
Route::get('pdfview',array('as'=>'pdfview','uses'=>'InvoiceController@pdfview'));
Route::get('/userlist', 'HomeController@userlist')->name('userlist');
Route::get('workorder', 'HomeController@workorder')->name('workorder');
Route::post('saveworkorder', 'InvoiceController@saveworkorder');
Route::get('getworkorder/{id}', 'HomeController@printworkorder')->name('getworkorder');
Route::get('workorderpdfview',array('as'=>'workorderpdfview','uses'=>'InvoiceController@workorderpdfview'));
Route::get('workorder_list', 'HomeController@workorder_list')->name('workorder_list');
Route::get('editworkorder/{id}', 'HomeController@editworkorder')->name('editworkorder');
Route::post('updateworkorder/{id}', 'InvoiceController@updateworkorder')->name('updateworkorder');
Route::get('salesreport', 'HomeController@salesreport')->name('salesreport');
Route::get('purchasereport', 'HomeController@purchasereport')->name('purchasereport');
Route::post('purchasereport', 'HomeController@purchasereport')->name('purchasereport.submit');
Route::post('edituser', 'InvoiceController@edituser')->name('edituser');
Route::get('purchaser', 'HomeController@purchaser')->name('purchaser');
Route::post('purchaser', 'HomeController@purchaser')->name('purchaser.submit');
Route::get('print-invoice/{id}', 'InvoiceController@printInvoice')->name('print-invoice');