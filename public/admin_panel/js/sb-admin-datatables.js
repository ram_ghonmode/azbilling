// Call the dataTables jQuery plugin
$(document).ready(function() {
    $('#cusdataTable').DataTable({
        scrollY: "298px",
        scrollX: true,
        scrollCollapse: true,
        paging: false,
        fixedColumns: {
            leftColumns: 2,
            rightColumns: 1
        }
    });

    $('#listInvodataTable').DataTable({
        scrollY: "340px",
        scrollX: true,
        scrollCollapse: true,
        fixedColumns: {
            leftColumns: 2,
            rightColumns: 1
        }
    });

    var table = $('#saledataTable').DataTable({
        scrollY: "298px",
        scrollX: true,
        scrollCollapse: true,
        paging: false,
        fixedColumns: {
            leftColumns: 2
        },
        lengthChange: false,
        dom: 'Bfrtip',
        buttons: [ 'copy',
        {
            extend: 'excel',
            text: 'Export Data',
            className: 'btn btn-info'
        }]
    });

    table.buttons().container()
        .appendTo( '#saledataTable_wrapper .col-md-6:eq(0)' );

    var purchaseTable = $('#reportDataTable').DataTable({
        scrollY: "298px",
        scrollX: true,
        scrollCollapse: true,
        paging: false,
        fixedColumns: {
            leftColumns: 2
        },
        lengthChange: false,
        dom: 'Bfrtip',
        buttons: [ 'copy',
        {
            extend: 'excel',
            text: 'Export Data',
            className: 'btn btn-info',
            exportOptions: {
                columns: 'th:not(.notinclude)'
            }
        }]
    });

    purchaseTable.buttons().container()
        .appendTo( '#reportDataTable_wrapper .col-md-6:eq(0)' );
});
