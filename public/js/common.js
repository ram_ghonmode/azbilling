var configHostname = window.location.hostname;
if(configHostname == 'localhost'){
    var configpath = {
        'globalPath': window.location.protocol + "//" + window.location.hostname + "/azbilling/"
    };
}else{
    var configpath = {
        'globalPath': window.location.protocol + "//" + window.location.hostname + "/"
    };
}


// customer details toggle
$(function(){
    $('#selectcustId').change(function(){
    	$("#custdetailId").show();
    	var selectedCustomOpt = $('option:selected', this).attr('data-indexval');
    	var custDetailadd = CustomerjsonToObj[selectedCustomOpt].address;
    	$("#CustaddId").text(custDetailadd);
    	var custDetailgstno = CustomerjsonToObj[selectedCustomOpt].gstinno;
    	$("#CustgstnId").text(custDetailgstno);
    });

    $('#userEditModalCenter').on('hidden.bs.modal', function () {
        $("#changePasswordDivId").hide();
        $("#nameId, #eUserEmailId, #eUserroleId, #eNewPassId, #eConfPassId, #eAdminPassId").val('');
        $("#userEditModalCenter").removeData("userData");
    });

    $("#selectPurcId").select2({
        placeholder: 'Select Vendor',
        width: '100%'
    });

    $('#invoice_dateId').datepicker({
        format: 'dd-mm-yyyy',
        startDate: new Date() 
    });
});

function showDomainRefFormDiv() {
    $("#domainRefrenceFormDivId").slideToggle("slow");
}

// Custom Js By Sankalp On 16 November, 2017
function showTaxAddFormDiv() {
    $("#TaxAddFormDivId").slideToggle("slow");
}

function showTaxGroupAddFormDiv() {
    $("#TaxAddGroupFormDivId").slideToggle("slow");
}

function openUserEditModal(userObj) {
    $("#userEditModalCenter").data("userData", userObj);
    var userData = $("#userEditModalCenter").data("userData");
    $("#userEditModalCenter span.text-info").text('( ' + userData.name +' )' );
    $("#nameId").val(userData.name);
    $("#eUserEmailId").val(userData.email);
    var roleName = (userData.role == 1) ? 'Admin' : 'User';
    $("#eUserroleId").val(roleName);
    $('#userEditModalCenter').modal('toggle');
}

function showChangePassDiv() {
    $("#changePasswordDivId").slideToggle("slow");
}

function saveEditUserInfo() {
    var Base64 = { _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function (e) { var t = ""; var n, r, i, s, o, u, a; var f = 0; e = Base64._utf8_encode(e); while (f < e.length) { n = e.charCodeAt(f++); r = e.charCodeAt(f++); i = e.charCodeAt(f++); s = n >> 2; o = (n & 3) << 4 | r >> 4; u = (r & 15) << 2 | i >> 6; a = i & 63; if (isNaN(r)) { u = a = 64 } else if (isNaN(i)) { a = 64 } t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a) } return t }, decode: function (e) { var t = ""; var n, r, i; var s, o, u, a; var f = 0; e = e.replace(/[^A-Za-z0-9+/=]/g, ""); while (f < e.length) { s = this._keyStr.indexOf(e.charAt(f++)); o = this._keyStr.indexOf(e.charAt(f++)); u = this._keyStr.indexOf(e.charAt(f++)); a = this._keyStr.indexOf(e.charAt(f++)); n = s << 2 | o >> 4; r = (o & 15) << 4 | u >> 2; i = (u & 3) << 6 | a; t = t + String.fromCharCode(n); if (u != 64) { t = t + String.fromCharCode(r) } if (a != 64) { t = t + String.fromCharCode(i) } } t = Base64._utf8_decode(t); return t }, _utf8_encode: function (e) { e = e.replace(/rn/g, "n"); var t = ""; for (var n = 0; n < e.length; n++) { var r = e.charCodeAt(n); if (r < 128) { t += String.fromCharCode(r) } else if (r > 127 && r < 2048) { t += String.fromCharCode(r >> 6 | 192); t += String.fromCharCode(r & 63 | 128) } else { t += String.fromCharCode(r >> 12 | 224); t += String.fromCharCode(r >> 6 & 63 | 128); t += String.fromCharCode(r & 63 | 128) } } return t }, _utf8_decode: function (e) { var t = ""; var n = 0; var r = c1 = c2 = 0; while (n < e.length) { r = e.charCodeAt(n); if (r < 128) { t += String.fromCharCode(r); n++ } else if (r > 191 && r < 224) { c2 = e.charCodeAt(n + 1); t += String.fromCharCode((r & 31) << 6 | c2 & 63); n += 2 } else { c2 = e.charCodeAt(n + 1); c3 = e.charCodeAt(n + 2); t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63); n += 3 } } return t } }

    var userDat = $("#userEditModalCenter").data("userData");
    var passDivStatus = $("#changePasswordDivId").attr('style');
    var sendData = {};
    if (passDivStatus == '' || passDivStatus == 'undefined' || passDivStatus == null){
        sendData = {
            'userId': userDat.id,
            'name': $("#nameId").val(),
            'pass':Base64.encode($("#eNewPassId").val()),
            'cpass':Base64.encode($("#eConfPassId").val()),
            'apass':Base64.encode($("#eAdminPassId").val()),
            'adminEmail': $("#eAdminEmailId").val(),
            '_token': $('meta[name="csrf-token"]').attr('content')
        };
    }else{
        sendData = {
            'userId': userDat.id,
            'name': $("#nameId").val(),
            'info': true
        };
    }

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: configpath['globalPath'] + 'edituser',
        data: sendData,
        dataType: 'json',
        async: true,
        type: 'post',
        success: function (response) {
            var alert = $('<div class="alert" role="alert">'+
                        '</div>');
            if (response.status) {
                $(alert).addClass('alert-success');
                $(alert).append(response.msg);
                setTimeout(function () {
                    window.location.reload();
                }, 2500);
            }else{
                $(alert).addClass('alert-danger');
                $(alert).append(response.msg);
            }
            $('#alertDivId').html(alert);
            setTimeout(function () { $('#alertDivId').html(''); }, 2000);
        }
    });
    // console.log(sendData);
}

function setNewImageToDisp(imgTag){
    if (imgTag.files && imgTag.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#previewImgId, #closeBtnId').removeClass('d-none');
            $('#previewImgId').attr('src', e.target.result);
        }

        reader.readAsDataURL(imgTag.files[0]);
    }else{
        $('#previewImgId, #closeBtnId').addClass('d-none');
        $('#previewImgId').attr('src', '');
    }
}

function removeSelectedImg(){
    $("#fileInput").val('');
    $("#fileInput").trigger('change');
}

function checkForInvoiceStatus(select) {
    var selected = $(select).val();
    if (selected == 'Paid') {
        $(select).attr('class', 'form-control bg-success text-light');
    } else {
        $(select).attr('class', 'form-control bg-danger text-light');
    }
}

function checkForInvoicePay(select) {
    var selected = $(select).val();
    if (selected == 'Cash' || selected == 'Card' || selected == 'Cheque' || selected == 'Online') {
        $(select).attr('class', 'form-control bg-success text-light');
    } else {
        $(select).attr('class', 'form-control bg-danger text-light');
    }
}

function editPurchaser(obj){
    $("#domainRefrenceFormDivId").show('slow');
    $("#editCancelBtnId").removeClass('d-none');
    $("input[name='vendor_name']").val(obj.vendor_name);
    $("input[name='address']").val(obj.address);
    $("input[name='email']").val(obj.email);
    $("input[name='gstinno']").val(obj.gstinno);
    $("input[name='contact']").val(obj.contact);
    $("#oldPurchaserId").val(obj.id);
    $("input[type='submit']").val('Update');
}

function closeEditUser(){
    $("#domainRefrenceFormDivId").hide('slow');
    $("#editCancelBtnId").addClass('d-none');
    $("#oldPurchaserId").val('');
    $("input[type='submit']").val('Save');
    $("input[name='vendor_name'], input[name='address'], input[name='email'], input[name='gstinno'], input[name='contact']").val('');
}

function editPurchaserEntry(obj){
    // $("html, body").animate({ scrollTop: 0 }, "slow");
    var d = new Date(obj.invoice_date);
    var dd = d.getDate();
    var mm = d.getMonth() + 1;
    var mmw = (mm <= 9) ? '0'+mm : mm;
    var yy = d.getFullYear();
    var newdate = dd + "-" + mmw + "-" + yy;
    $("#oldPurchaseItemId").val(obj.id);
    $("#domainRefrenceFormDivId").show('slow');
    $("#editCancelBtnId").removeClass('d-none');
    $("input[type='submit']").val('Update');
    $("#selectPurcId").val(obj.purchaser_id);
    $("#selectPurcId").trigger('change');
    $("input[name='invoice_no']").val(obj.invoice_no);
    $("input[name='invoice_date']").val(newdate);
    $("input[name='invoice_value']").val(obj.invoice_value);
    $("input[name='rate']").val(obj.rate);
    $("input[name='taxable_value']").val(obj.taxable_value);
    $("input[name='cgst']").val(obj.cgst);
    $("input[name='sgst']").val(obj.sgst);
    $("input[name='igst']").val(obj.igst);
    $("#descpId").text(obj.description);
    $("#remarkId").text(obj.remark);
}

function closeEditPurchaseEntry(){
    $("#domainRefrenceFormDivId").hide('slow');
    $("#editCancelBtnId").addClass('d-none');
    $("input[type='submit']").val('Save');
    $("#oldPurchaseItemId, #selectPurcId").val('');
    $("#descpId, #remarkId").text('');
    $("#selectPurcId").trigger('change');
    $("input[name='invoice_no'], input[name='invoice_date'], input[name='invoice_value'], input[name='rate'], input[name='taxable_value'], input[name='cgst'], input[name='sgst'], input[name='igst']").val('');
}