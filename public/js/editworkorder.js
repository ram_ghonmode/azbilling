$(document).ready(function(){
    calc();
    $('#invoice_dateId').datepicker();
    //search inputbox
    $( function() {
        $("#selectcustId").select2();
    });

    if(typeof TaxjsonToObj == 'undefined'){
        TaxjsonToObj = [];
    }

    var taxObjOption = "<select class='form-control' onchange='findTaxesFromTaxGroup(this)'><option value='0'></option>";
    $.each(TaxjsonToObj, function(i, n){
        taxObjOption += "<option value='"+n.id+"' id='tax_"+i+"' data-taxes='"+i+"'>"+n.name+"</option>";
    });
    taxObjOption += "</select>";

    var rowCount = $('#tabCount tr').length;
    var i= ++rowCount;
    $(".addrow").on('click',function(){
    	var data = "<tr>" +
    					"<td>" + i + "</td>" +
    	    			"<td><input type='text' id='desc_"+i+"' name='' class='form-control'/></td>" +
    	    			"<td><input type='number' name='qty' id='qty_"+i+"' class='form-control qty key'/></td>" +
    	    			"<td><input type='number' name='price' id='price_"+i+"' class='form-control price key'/></td>" +
    	    			"<td>"+ taxObjOption +"</td>" +
    	    			"<td><input type='number' id='item_total_"+i+"' class='form-control total' readonly/>" +
    	    			"</td>" +
    	    			"<td>" +
    	    				"<button type='button' class='btn btn-danger deleteBtn'><i class='fa fa-times' aria-hidden='true'></i></button>" +
    	    			"</td>" +
    	    		"</tr>";
    	    $('#splashTable').append(data);
    	    i++;
    });

    $(document).on("click", ".deleteBtn", function(){
        $(this).closest("tr").remove();
        $("#splashTable").find("tbody").find("tr").each(function(i, v){
            $(v).find("td:eq(0)").text(++i);
            $(v).find("td:eq(1)").find('input').attr('id', 'desc_' + i);
            $(v).find("td:eq(2)").find('input').attr('id', 'qty_' + i);
            $(v).find("td:eq(3)").find('input').attr('id', 'price_' + i);
            $(v).find("td:eq(4)").find('select').attr('data-taxes', 'tax_' + i);
            $(v).find("td:eq(5)").find('input').attr('id', 'item_total_' + i);
        });
    });

    $(document).on("keyup keypress blur change", ".key", function(){
        $("#splashTable").find("tbody").find("tr").each(function(i, v){
            var serialno = $(v).find("td:eq(0)").text();
            var selectedtaxvalue = $(v).find("td:eq(4)").find('select').val();
            if(selectedtaxvalue != 0){
                calc();
            }
        });
    });

    $("#submitWorkorderId").click(function(){
        if(checkInvoiceValidation()){
            var workorderItems = [];
            $("#splashTable").find("tbody").find("tr").each(function(i, v){
                var pdata = {
                    'description' : $(v).find("td:eq(1)").find('input').val(),
                    'qty' : $(v).find("td:eq(2)").find('input').val(),
                    'unit_price' : $(v).find("td:eq(3)").find('input').val(),
                    'tax_price' : parseFloat($(v).find("td:eq(5)").find('input').val()) - parseFloat(parseFloat($(v).find("td:eq(3)").find('input').val()) * parseFloat($(v).find("td:eq(2)").find('input').val())),
                    'total' : $(v).find("td:eq(5)").find('input').val(),
                    'taxgroup_id' : $(v).find("td:eq(4)").find('select').val(),
                };
                workorderItems.push(pdata);
            });
            var sdata = {
                'customer_id' : $("#selectcustId").val(),
                'workorder_date': $("#invoice_dateId").val(),
                'total' : $("#subtotalId").text(),
                'tax_price' : $("#taxtotalId").text(),
                'grand_total' : $("#gtotalId").text(),
                'workorde_items' : workorderItems,
                '_token' : $('meta[name="csrf-token"]').attr('content')
            };
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: configpath['globalPath'] + 'updateworkorder/' + $("#editWorkorderId").val(),
                data: sdata,
                dataType: 'json',
                async: true,
                type: 'post',
                success: function(response){
                    if(response.status){
                        window.location = configpath['globalPath'] + 'getworkorder/' + response.id;
                    }
                }
            });
        }
    });
});

function calc(){
    var GrandTotal = 0;
    var TaxTotal = 0;
    var SubTotal = 0;
    $("#splashTable").find("tbody").find("tr").each(function(i, v){
        var qty = $(v).find("td:eq(2)").find('input').val();
        var price = $(v).find("td:eq(3)").find('input').val();
        var tax = $(v).find("td:eq(4)").find('option:selected').attr('data-taxes');
        var TotalWithOouTax = qty * price;
        var TotalWithTax = FinalTotalTr = Taxprice = subtotal = 0;
        if($(v).find("td:eq(4)").find('option:selected').val() != 0){
            var FromTaxValue = TaxjsonToObj[tax];
            $.each(FromTaxValue.taxes, function(it, vt){
                TotalWithTax += TotalWithOouTax * vt.tax_rate / 100;
            });
        }
        FinalTotalTr = parseFloat(TotalWithOouTax) + parseFloat(TotalWithTax);

        $(v).find("td:eq(5)").find('input').val(FinalTotalTr.toFixed(2));
        GrandTotal += parseFloat(FinalTotalTr.toFixed(2));
        TaxTotal += parseFloat(TotalWithTax.toFixed(2));
        SubTotal += parseFloat(TotalWithOouTax.toFixed(2));
    });
    $("#gtotalId").text(GrandTotal.toFixed(2));
    $("#taxtotalId").text(TaxTotal.toFixed(2));
    $("#subtotalId").text(SubTotal.toFixed(2));
}

function findTaxesFromTaxGroup(that){
    if($(that).val() != 0){
        calc();
    }
}

function checkInvoiceValidation(){
    var CustomerId = $("#selectcustId").val();
    if(CustomerId == '' || CustomerId == null || CustomerId == 'undefined'){
        alert('Please Select Customer First');
        return false;
    }

    var InvoiceDateId = $("#invoice_dateId").val();
    if(InvoiceDateId == '' || InvoiceDateId == null || InvoiceDateId  == 'undefined'){
        alert('Please Select Date');
        return false;
    }
    $("#splashTable").find("tbody").find("tr").each(function(i, v){
        breakOut = false;
        var descrip = $(v).find("td:eq(1)").find('input').val();
        console.log(descrip);
        if( descrip == '' || descrip == null || descrip == 'undefined'){
            alert('Please Enter description First');
            breakOut = true;
            return false;
        }
        var qty = $(v).find("td:eq(2)").find('input').val();
        if( qty == '' || qty == null || qty == 'undefined'){
            alert('Please Enter Quantity First');
            breakOut = true;
            return false;
        }
        var unit_price = $(v).find("td:eq(3)").find('input').val();
        if( unit_price == '' || unit_price == null || unit_price == 'undefined'){
            alert('Please Enter Unit Price First');
            breakOut = true;
            return false;
        }
        var taxgroup_id = $(v).find("td:eq(4)").find('select').val();
        if( taxgroup_id == '' || taxgroup_id == null || taxgroup_id == 'undefined' || taxgroup_id == 0){
            alert('Please Select Tax Group First');
            breakOut = true;
            return false;
        }
        var total = $(v).find("td:eq(5)").find('input').val();
        if( total == '' || total == null || total == 'undefined'){
            alert('Total is Not Calculated.. Please Check');
            breakOut = true;
            return false;
        }
    });
    if(breakOut){
        return false;
    }
    return true;
}