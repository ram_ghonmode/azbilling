ALTER TABLE `invoices` ADD `status` ENUM('Paid','Pending') NOT NULL DEFAULT 'Pending' AFTER `grand_total`;

ALTER TABLE `invoice_items` CHANGE `qty` `qty` VARCHAR(60) NOT NULL, CHANGE `unit_price` `unit_price` VARCHAR(60) NOT NULL, CHANGE `tax_price` `tax_price` VARCHAR(60) NOT NULL, CHANGE `total` `total` VARCHAR(60) NULL DEFAULT NULL;
ALTER TABLE `workorde_items` CHANGE `qty` `qty` VARCHAR(60) NULL DEFAULT NULL, CHANGE `unit_price` `unit_price` VARCHAR(60) NULL DEFAULT NULL, CHANGE `tax_price` `tax_price` VARCHAR(60) NULL DEFAULT NULL, CHANGE `total` `total` VARCHAR(60) NULL DEFAULT NULL;
ALTER TABLE `invoices` CHANGE `tax_price` `tax_price` VARCHAR(60) NULL DEFAULT NULL, CHANGE `grand_total` `grand_total` VARCHAR(60) NULL DEFAULT NULL;
ALTER TABLE `workorders` CHANGE `tax_price` `tax_price` VARCHAR(60) NULL DEFAULT NULL, CHANGE `grand_total` `grand_total` VARCHAR(60) NULL DEFAULT NULL;

ALTER TABLE `workorders` CHANGE `workorder_date` `workorder_date` DATE NULL;