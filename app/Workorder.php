<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workorder extends Model
{
    protected $table = 'workorders';

    public function workorde_items(){
		return $this->hasMany('App\Workorde_item', 'workorder_id');
	}

	public function customers(){
		return $this->belongsTo('App\Customer', 'customer_id');
	}

	public function domains(){
		return $this->belongsTo('App\Domain', 'domain_id');
	}
}
