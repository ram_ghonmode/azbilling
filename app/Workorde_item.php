<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workorde_item extends Model
{
    protected $table = 'workorde_items';

    public function workorders(){
		return $this->belongsTo('App\Workorder', 'workorder_id');
	}

	public function groups(){
		return $this->belongsTo('App\Group', 'taxgroup_id');
	}
}
