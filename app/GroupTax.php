<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupTax extends Model{
    protected $table = 'group_tax';
    
    public function groups(){
        return $this->belongsToMany('App\Group');
    }

    public function taxes(){
        return $this->belongsToMany('App\Tax');
    }
}