<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
	protected $table = 'groups';

    public function taxes(){
        return $this->belongsToMany('App\Tax');
    }

    public function invoice_items(){
    	return $this->hasOne('App\Invoice_item');
    }

    public function workorde_items(){
    	return $this->hasOne('App\Workorde_item');
    }
}
