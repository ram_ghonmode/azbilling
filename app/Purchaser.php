<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchaser extends Model
{
    protected $table = 'purchasers';

    public function purchase_items(){
		return $this->hasMany('App\Purchase_item', 'purchaser_id');
	}

    public function domains(){
        return $this->belongsTo(Domain::class);
    }
}
