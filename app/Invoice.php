<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoices';

	public function invoice_items(){
		return $this->hasMany('App\Invoice_item', 'invoice_id');
	}

	public function customers(){
		return $this->belongsTo('App\Customer', 'customer_id');
	}

	public function domains(){
		return $this->belongsTo('App\Domain', 'domain_id');
	}
}
