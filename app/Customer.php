<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
	protected $table = 'customers';

    public function domains(){
        return $this->belongsTo(Domain::class);
    }

    public function invoices(){
    	return $this->hasOne('App\Invoice');
    }

    public function workorders(){
    	return $this->hasOne('App\Workorder');
    }
}
