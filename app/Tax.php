<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    public function groups(){
        return $this->belongsToMany('App\Group');
    }
}
