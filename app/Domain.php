<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
	protected $table = 'domains';

    public function customers(){
    	return $this->hasMany(Customer::class, 'id');
    }

    public function invoices(){
    	return $this->hasMany(Invoice::class, 'id');
    }

    public function workorders(){
    	return $this->hasMany(Workorder::class, 'id');
    }

    public function purchasers(){
    	return $this->hasMany(Purchaser::class, 'id');
    }

    public function purchase_items(){
    	return $this->hasMany(Purchase_item::class, 'id');
    }
}
