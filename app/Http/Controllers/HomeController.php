<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use PDF;
use App\Domain;
use App\Tax;
use App\Group;
use App\Customer;
use App\Invoice;
use App\Invoice_item;
use App\User;
use App\Workorder;
use App\Workorde_item;
use App\Purchaser;
use App\Purchase_item;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $lastId = Invoice::select('id','voucher_no')->where('domain_id', 3)->orderBy('id', 'DESC')->first();
        // $str = $lastId->voucher_no;
        // $strArr = explode("-",$str);
        // $str_last_val = end($strArr);
        // $str_last_num = preg_replace("/[^0-9,.]/", "", $str_last_val);
        // $id = ($str_last_num + 1);
        // $invunivoucherId = sprintf("%'.04d\n", $id);
        // dd(preg_replace('/\s+/', '', $invunivoucherId));
        $data['customercount'] = Customer::where('is_deleted',0)->count();
        $data['invoicecount'] = Invoice::where('is_deleted',0)->count();
        $data['workordercount'] = Workorder::where('is_deleted',0)->count();
        $data['vendorcount'] = Purchaser::where('is_deleted',0)->count();
        return view('home', compact('data'));
    }

    public function domain(Request $request){
        if($request->isMethod('post')){
            if(!empty(Auth::user()->domain_id)){
                $this->validate($request, array(
                    'name' => 'required',
                    'address' => 'required',
                    'mobile' => 'required',
                    'customer_prefix' => 'required',
                    'invoice_prefix' => 'required',
                ));
                $input = $request->all();
                $data = array(
                    'name' => $input['name'],
                    'address' => $input['address'],
                    'mobile' => $input['mobile'],
                    'mobile2' => $input['mobile2'],
                    'landline' => $input['landline'],
                    'email' => $input['email'],
                    'website' => $input['website'],
                    'customer_prefix' => $input['customer_prefix'],
                    'invoice_prefix' => $input['invoice_prefix'],
                    'gstinno' => $input['gstinno'],
                    'pan_no' => $input['pan_no'],
                    'bankaccno' => $input['bankaccno'],
                    'bankname' => $input['bankname'],
                    'ifsc' => $input['ifsc'],
                );
                if(isset($input['logo'])){
                    $data['logo'] = $this->imageupload($request->file('logo'), 'domaindata');
                }
                if(Domain::where('id', Auth::user()->domain_id)->update($data)){
                    $domaindata = Domain::find(Auth::user()->domain_id);
                    return view('dashboard.domain',['domaindata' => $domaindata])->with('success', 'Company Detail Successfully Updated.');
                }
            }else{
                $this->validate($request, array(
                    'name' => 'required|unique:domains',
                    'address' => 'required',
                    'mobile' => 'required|digits:10',
                    'logo' => 'required|image|mimes:jpg,jpeg|max:2048',
                    'customer_prefix' => 'required',
                    'invoice_prefix' => 'required',
                ));
                $input = $request->all();
                $domain = new Domain;
                $domain->name = $input['name'];
                $domain->address = !empty($input['address']) ? $input['address'] : null;
                $domain->mobile = !empty($input['mobile']) ? $input['mobile'] : null;
                $domain->mobile2 = !empty($input['mobile2']) ? $input['mobile2'] : null;
                $domain->landline = !empty($input['landline']) ? $input['landline'] : null;
                $domain->email = !empty($input['email']) ? $input['email'] : null;
                $domain->logo = !empty($request->file('logo')) ? $this->imageupload($request->file('logo'), 'logo') : null;
                $domain->website = !empty($input['website']) ? $input['website'] : null;
                $domain->customer_prefix = !empty($input['customer_prefix']) ? $input['customer_prefix'] : null;
                $domain->invoice_prefix = !empty($input['invoice_prefix']) ? $input['invoice_prefix'] : null;
                $domain->gstinno = !empty($input['gstinno']) ? $input['gstinno'] : null;
                $domain->pan_no = !empty($input['pan_no']) ? $input['pan_no'] : null;
                $domain->bankaccno = !empty($input['bankaccno']) ? $input['bankaccno'] : null;
                $domain->bankname = !empty($input['bankname']) ? $input['bankname'] : null;
                $domain->ifsc = !empty($input['ifsc']) ? $input['ifsc'] : null;
                if($domain->save()){
                    $data = array(
                        'domain_id' => $domain->id,
                    );
                    if(User::where('id', Auth::id())->update($data)){
                        return view('dashboard.domain')->with('success', 'Company Detail Successfully Saved. Now Click on Dashboard');
                    }
                }
            }
        }
        $domaindata = NULL;
        $id = Auth::user()->domain_id;
        if(!empty($id)){
            $domaindata = Domain::find($id);
        }
        return view('dashboard.domain',['domaindata' => $domaindata]);
    }

    public function userlist(){
        if(Auth::user()->role == 1){
            $alluser = $this->singlefunctionforquery('user');
            return view('dashboard.userlist',['alluser' => $alluser]);
        }else{
            return view('home');
        }
    }

    public function taxes(Request $request){
        if(Auth::user()->role == 1){
            if($request->isMethod('post')){
                $this->validate($request, array(
                    'name' => 'required|unique:taxes',
                    'tax_rate' => 'required',
                ));
                $input = $request->all();
                $tax = new Tax;
                $tax->name = $input['name'];
                $tax->description = !empty($input['description']) ? $input['description'] : null;
                $tax->tax_number = !empty($input['tax_number']) ? $input['tax_number'] : null;
                $tax->tax_rate = $input['tax_rate'];
                $tax->effective_from = !empty($input['effective_from']) ? date('Y-m-d', strtotime($input['effective_from'])) : null;
                $tax->effective_till = !empty($input['effective_till']) ? date('Y-m-d', strtotime($input['effective_till'])) : null;
                if($tax->save()){
                    $alltaxes = $this->singlefunctionforquery('tax');
                    return view('dashboard.taxes',['taxes' => $alltaxes])->with('success', 'Tax Detail Successfully Saved');
                }
            }
            $alltaxes = $this->singlefunctionforquery('tax');
            return view('dashboard.taxes',['taxes' => $alltaxes]);
        }else{
            return view('home');
        }
    }

    public function edittax($id, Request $request){
        if(Auth::user()->role == 1){
            if($request->isMethod('post')){
                $this->validate($request, array(
                    'name' => 'required',
                    'tax_rate' => 'required',
                ));
                $input = $request->all();
                $data = array(
                    'name' => $input['name'],
                    'description' => $input['description'],
                    'tax_number' => $input['tax_number'],
                    'tax_rate' => $input['tax_rate'],
                    'effective_from' => !empty($input['effective_from']) ? date('Y-m-d', strtotime($input['effective_from'])) : null,
                    'effective_till' => !empty($input['effective_till']) ? date('Y-m-d', strtotime($input['effective_till'])) : null,
                );
                if(Tax::where('id', $id)->update($data)){
                    $tax = Tax::find($id);
                    return view('dashboard.edittax', ['tax' => $tax])->with('success', 'Tax Successfully Updated.');
                }else{
                    return Redirect::back()->withErrors(['msg' => 'Unable To Update Tax.. Try Again']);
                }
            }
            $tax = Tax::find($id);
            return view ('dashboard.edittax', ['tax' => $tax]);
        }else{
            return view('home');
        }
    }

    public function taxgroup(Request $request){
        if(Auth::user()->role == 1){
            if($request->isMethod('post')){
                $this->validate($request, array(
                    'name' => 'required|unique:groups',
                ));
                $input = $request->all();
                $group = new Group;
                $group->name = $input['name'];
                $group->description = $input['description'];
                if($group->save()){
                    $group->taxes()->sync($input['tax']);
                    $alltaxesngroup = $this->singlefunctionforquery('groupntax');
                    return view('dashboard.taxgroup',['alltaxesngroup' => $alltaxesngroup])->with('success', 'Tax Group Successfully Saved');
                }
            }
            $alltaxesngroup = $this->singlefunctionforquery('groupntax');
            return view('dashboard.taxgroup',['alltaxesngroup' => $alltaxesngroup]);
        }else{
            return view('home');
        }
    }

    public function edittaxgroup($id, Request $request){
        if(Auth::user()->role == 1){
            if($request->isMethod('post')){
                $input = $request->all();
                $data = array(
                    'name' => $input['name'],
                    'description' => $input['description'],
                );
                if(Group::where('id', $id)->update($data)){
                    $group = new Group;
                    $group->id = $id;
                    $group->taxes()->sync($input['tax']);
                    $alltax = Tax::where('is_deleted', 0)
                            ->select(['taxes.id','taxes.name'])
                            ->orderBy('id','DESC')
                            ->get();
                    $taxgroup = Group::with(['taxes' => function($query){$query->select('taxes.id')->where('is_deleted', 0);},])->where('id', $id)->first();
                    return view ('dashboard.edittaxgroup', ['taxgroup' => $taxgroup, 'alltax' => $alltax])->with('success', 'Tax Group Successfully Updated');
                }else{
                    return Redirect::back()->withErrors(['msg' => 'Unable To Update Group Tax.. Try Again']);
                }
            }
            $alltax = Tax::where('is_deleted', 0)
                            ->select(['taxes.id','taxes.name'])
                            ->orderBy('id','DESC')
                            ->get();
            $taxgroup = Group::with(['taxes' => function($query){$query->select('taxes.id')->where('is_deleted', 0);},])->where('id', $id)->first();
            return view ('dashboard.edittaxgroup', ['taxgroup' => $taxgroup, 'alltax' => $alltax]);
        }else{
            return view('home');
        }
    }

    public function customer(Request $request){
        if($request->isMethod('post')){
            $this->validate($request, array(
                'client_name' => 'required',
                'contact' => 'required',
            ));
            $lastId = Customer::select('id')->where('domain_id', Auth::user()->domain_id)->orderBy('id', 'DESC')->first();
            if(empty($lastId)){
                $custlastId = 0;
            }else{
                $custlastId = $lastId->id;
            }
            $input = $request->all();
            $customer = new Customer;
            $customer->client_no = $this->customeruniqueid($custlastId);
            $customer->client_name = $input['client_name'];
            $customer->domain_id = Auth::user()->domain_id;
            $customer->address = $input['address'];
            $customer->customer_name = $input['customer_name'];
            $customer->email = $input['email'];
            $customer->contact = $input['contact'];
            $customer->gstinno = $input['gstinno'];
            if($customer->save()){
                $allcustomer = $this->singlefunctionforquery('customer');
                return view('dashboard.customer',['customer' => $allcustomer])->with('success', 'Customer Detail Successfully Saved');
            }
        }
        $allcustomer = $this->singlefunctionforquery('customer');
        return view('dashboard.customer',['customer' => $allcustomer]);
    }

    public function editcustomer(Request $request,$id){
        if($request->isMethod('post')){
            $input = $request->all();
            $data = array(
                'client_name' => $input['client_name'],
                'address' => $input['address'],
                'customer_name' => $input['customer_name'],
                'email' => $input['email'],
                'contact' => $input['contact'],
                'gstinno' => $input['gstinno'],
            );
            Customer::where('id', $id)->update($data);
            return Redirect::back()->withErrors(['success' => 'Customer Update Successfully']);
        }
        $customer = Customer::find($id);
        return view('dashboard.editcustomer',['customer' => $customer]);
    }

    public function purchaser(Request $request){
        if($request->isMethod('post')){
            $this->validate($request, array(
                'vendor_name' => 'required',
                'contact' => 'required',
            ));
            $input = $request->all();
            if(!empty($input['purchaserId'])){
                $data = array(
                    'vendor_name' => $input['vendor_name'],
                    'address' => $input['address'],
                    'email' => $input['email'],
                    'contact' => $input['contact'],
                    'gstinno' => $input['gstinno'],
                );
                if(Purchaser::where('id', $input['purchaserId'])->update($data)){
                    $allpurchaser = $this->singlefunctionforquery('purchaser');
                    return view('dashboard.purchaser',['purchaser' => $allpurchaser])->with('success', 'Purchaser Detail Successfully Updated');
                }
            }else{
                $purchaser = new Purchaser;
                $purchaser->domain_id = Auth::user()->domain_id;
                $purchaser->address = $input['address'];
                $purchaser->vendor_name = $input['vendor_name'];
                $purchaser->email = $input['email'];
                $purchaser->contact = $input['contact'];
                $purchaser->gstinno = $input['gstinno'];
                if($purchaser->save()){
                    $allpurchaser = $this->singlefunctionforquery('purchaser');
                    return view('dashboard.purchaser',['purchaser' => $allpurchaser])->with('success', 'Purchaser Detail Successfully Saved');
                }
            }
        }
        $allpurchaser = $this->singlefunctionforquery('purchaser');
        return view('dashboard.purchaser', ['purchaser' => $allpurchaser]);
    }

    public function invoice(Request $request){
        $alldomain = Domain::where('id', Auth::user()->domain_id)->select(['id','name','gstinno'])->first();
        $allcustomer = $this->singlefunctionforquery('customer');
        $alltaxes = Group::with(['taxes' => function($query){$query->select('taxes.id','taxes.name','taxes.tax_rate')->where('is_deleted', 0);},])->get();
        return view('dashboard.invoice',['alldomain' => $alldomain, 'allcustomer' => $allcustomer, 'alltaxes' => $alltaxes]);
    }

    public function invoice_list(){
        $allinvoicelist = Invoice::with(['customers'])->where(['is_deleted' => 0, 'domain_id' => Auth::user()->domain_id])->orderBy('id','DESC')->get();
        return view('dashboard.invoice_list',['allinvoicelist' => $allinvoicelist]);
    }

    public function editinvoice($id){
        $allinvoicedata = Invoice::with(['invoice_items' => function($query){$query->with('groups.taxes');},'customers','domains'])->where(['is_deleted' => 0 , 'id' => $id ])->first();
        $allcustomer = $this->singlefunctionforquery('customer');
        $allgroups = Group::select('groups.id','groups.name')->get();
        $alltaxes = Group::with(['taxes' => function($query){$query->select('taxes.id','taxes.name','taxes.tax_rate')->where('is_deleted', 0);},])->get();
        return view('dashboard.editinvoice',['allinvoicedata' => $allinvoicedata, 'allcustomer'=> $allcustomer,'allgroups' => $allgroups, 'alltaxes' => $alltaxes]);
    }

    public function printinvoice($id = null){
        if(!empty($id)){
            $allinvoicedata = Invoice::with(['invoice_items' => function($query){$query->with('groups.taxes');},'customers','domains'])->where(['is_deleted' => 0 , 'id' => $id ])->first();
            // dd($allinvoicedata);
        }
        return view('dashboard.getinvoice',[ 'allinvoicedata' => $allinvoicedata]);
    }

    public function workorder(){
        $alldomain = Domain::where('id', Auth::user()->domain_id)->select(['id','name','gstinno'])->first();
        $allcustomer = $this->singlefunctionforquery('customer');
        $alltaxes = Group::with(['taxes' => function($query){$query->select('taxes.id','taxes.name','taxes.tax_rate')->where('is_deleted', 0);},])->get();
        return view('dashboard.workorder',['alldomain' => $alldomain, 'allcustomer' => $allcustomer, 'alltaxes' => $alltaxes]);
    }

    public function printworkorder($id = null){
        if(!empty($id)){
            $allworkorderdata = Workorder::with(['workorde_items' => function($query){$query->with('groups.taxes');},'customers','domains'])->where(['is_deleted' => 0 , 'id' => $id ])->first();
        }
        return view('dashboard.getworkorder',['allworkorderdata' => $allworkorderdata]);
    }

    public function workorder_list(){
        $allworkorderlist = Workorder::with(['customers'])->where(['is_deleted' => 0, 'domain_id' => Auth::user()->domain_id])->orderBy('id','DESC')->get();
        return view('dashboard.workorder_list',['allworkorderlist' => $allworkorderlist]);
    }

    public function editworkorder($id){
        $allworkorderdata = Workorder::with(['workorde_items' => function($query){$query->with('groups.taxes');},'customers','domains'])->where(['is_deleted' => 0 , 'id' => $id ])->first();
        $allcustomer = $this->singlefunctionforquery('customer');
        $allgroups = Group::select('groups.id','groups.name')->get();
        $alltaxes = Group::with(['taxes' => function($query){$query->select('taxes.id','taxes.name','taxes.tax_rate')->where('is_deleted', 0);},])->get();
        return view('dashboard.editworkorder',['allworkorderdata' => $allworkorderdata, 'allcustomer'=> $allcustomer,'allgroups' => $allgroups, 'alltaxes' => $alltaxes]);
    }

    public function salesreport(){
        $allinvoices = Invoice::with(['invoice_items' => function($query){$query->with('groups.taxes');},
                                    'customers' => function($query){$query->select('customers.id','customers.client_name','customers.gstinno','customers.address');}])
                                    ->where(['is_deleted' => 0 , 'domain_id' => Auth::user()->domain_id ])->orderBy('invoice_date','DESC')->get();
        return view('dashboard.salesreport', ['allinvoices' => $allinvoices]);
    }

    public function purchasereport(Request $request){
        if($request->isMethod('post')){
            $input = $request->all();
            $msg = null;
            if(!empty($input['invoice_date']) && !empty($input['invoice_value'])){
                $this->validate($request, array(
                    'purchaser_id' => 'required',
                    'invoice_no' => 'required',
                    'invoice_value' => 'required'
                ));
                if(!empty($input['purchaseitemId'])){
                    $data = array(
                        'purchaser_id' => $input['purchaser_id'],
                        'invoice_date' => !empty($input['invoice_date']) ? date('Y-m-d', strtotime($input['invoice_date'])) : null,
                        'invoice_no' => $input['invoice_no'],
                        'invoice_value' => $input['invoice_value'],
                        'description' => $input['description'],
                        'rate' => $input['rate'],
                        'taxable_value' => $input['taxable_value'],
                        'cgst' => $input['cgst'],
                        'sgst' => $input['sgst'],
                        'igst' => $input['igst'],
                        'remark' => $input['remark'],
                    );
                    if(Purchase_item::where('id', $input['purchaseitemId'])->update($data)){
                        $msg = 'Purchase items successfully updated.!';
                    }
                }else{
                    $purchaseItem = new Purchase_item;
                    $purchaseItem->purchaser_id = $input['purchaser_id'];
                    $purchaseItem->domain_id = Auth::user()->domain_id;
                    $purchaseItem->invoice_date = isset($input['invoice_date']) ? date('Y-m-d', strtotime($input['invoice_date'])) : null;
                    $purchaseItem->invoice_no = $input['invoice_no'];
                    $purchaseItem->invoice_value = $input['invoice_value'];
                    $purchaseItem->description = $input['description'];
                    $purchaseItem->rate = $input['rate'];
                    $purchaseItem->taxable_value = $input['taxable_value'];
                    $purchaseItem->cgst = $input['cgst'];
                    $purchaseItem->sgst = $input['sgst'];
                    $purchaseItem->igst = $input['igst'];
                    $purchaseItem->remark = $input['remark'];
                    if($purchaseItem->save()){
                        $msg = 'Purchase items successfully added.!';
                    }
                }
            }else{
                // here is code for start date and end date report.
            }
        }
        $allpurchasers = Purchaser::select(['purchasers.id','purchasers.vendor_name','purchasers.domain_id'])
                                 ->orderBy('vendor_name','ASC')
                                 ->where(['is_deleted' => 0 , 'domain_id' => Auth::user()->domain_id ])
                                 ->get();

        $purchaseItems = Purchase_item::where(['is_deleted' => 0 , 'domain_id' => Auth::user()->domain_id ])
                                    ->with(['purchasers' => function($query){$query->select('purchasers.id','purchasers.vendor_name','purchasers.domain_id','purchasers.gstinno');}])
                                    ->orderBy('invoice_date','DESC')
                                    ->get();

        if(!empty($msg)){
            return view('dashboard.purchasereport', ['allpurchasers' => $allpurchasers , 'purchaseItems' => $purchaseItems])->with('success', $msg);
        }else{
            return view('dashboard.purchasereport', ['allpurchasers' => $allpurchasers , 'purchaseItems' => $purchaseItems]);
        }
    }

    protected function customeruniqueid($id){
        $companyData = Domain::find(Auth::user()->domain_id);
        $initialLabel = $companyData->customer_prefix;
        $alfabet = "A";
        $customerId = $initialLabel.'-'.$alfabet.sprintf("%'.03d\n",++$id);
        $customerId = preg_replace('/\s+/', '', $customerId);
        return $customerId;
    }

    public function delete($id, $model){
        switch ($model) {
            case 'Customer':
                $cust = Customer::find($id);
                $data = array(
                    'is_deleted' => 1,
                );
                if(Customer::where('id', $id)->update($data)){
                    return Redirect::back()->withErrors(['smsg' => 'Customer Deleted Successfully']);
                }else{
                    return Redirect::back()->withErrors(['fmsg' => 'Failed To Delete Customer. Try Again!!!']);
                }
            break;

            case 'Invoice':
                $cust = Invoice::find($id);
                $data = array(
                    'is_deleted' => 1,
                );
                if(Invoice::where('id', $id)->update($data)){
                    return Redirect::back()->withErrors(['sdmsg' => 'Invoice Deleted Successfully']);
                }else{
                    return Redirect::back()->withErrors(['fdmsg' => 'Failed To Delete Invoice. Try Again!!!']);
                }
            break;

            case 'Workorder':
                $cust = Workorder::find($id);
                $data = array(
                    'is_deleted' => 1,
                );
                if(Workorder::where('id', $id)->update($data)){
                    return Redirect::back()->withErrors(['sdmsg' => 'Workorder Deleted Successfully']);
                }else{
                    return Redirect::back()->withErrors(['fdmsg' => 'Failed To Delete Workorder. Try Again!!!']);
                }
            break;

            case 'Purchaser':
                $cust = Purchaser::find($id);
                $data = array(
                    'is_deleted' => 1,
                );
                if(Purchaser::where('id', $id)->update($data)){
                    return Redirect::back()->withErrors(['smsg' => 'Purchaser Deleted Successfully']);
                }else{
                    return Redirect::back()->withErrors(['fmsg' => 'Failed To Delete Purchaser. Try Again!!!']);
                }
            break;

            case 'Purchase_item':
                $cust = Purchase_item::find($id);
                $data = array(
                    'is_deleted' => 1,
                );
                if(Purchase_item::where('id', $id)->update($data)){
                    return Redirect::back()->withErrors(['smsg' => 'Purchase_item Deleted Successfully']);
                }else{
                    return Redirect::back()->withErrors(['fmsg' => 'Failed To Delete Purchase_item. Try Again!!!']);
                }
            break;

        }
    }

    protected function imageupload($selectedimage, $folder){
        $imgpath = null;
        $image = $selectedimage;
        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/images/'.$folder);
        if($image->move($destinationPath, $input['imagename'])){
            $imgpath = url('/public/images/'.$folder)."/".$input['imagename'];
        }
        return $imgpath;
    }

    protected function singlefunctionforquery($query){
        switch ($query) {
            case 'domain':
                $alldomain = Domain::where('is_deleted', 0)
                                    ->select(['domains.id','domains.name','domains.short_name','domains.created_at'])
                                    ->orderBy('id','DESC')
                                    ->get();
                return $alldomain;
            break;

            case 'user':
                // $matchThese = ['role' => 2];
                $alluser = User::select(['users.id','users.name','users.email','users.role'])
                                 ->orderBy('id','DESC')
                                 ->get();
                return $alluser;
            break;

            case 'tax':
                $alltaxes = Tax::where('is_deleted', 0)
                                ->select(['taxes.id','taxes.name','taxes.description','taxes.tax_number','taxes.tax_rate','taxes.effective_from','taxes.effective_till'])
                                ->orderBy('id','DESC')
                                ->get();
                return $alltaxes;
            break;

            case 'groupntax':
                $allgroup = Group::with(['taxes' => function($query){$query->select('taxes.id','taxes.name','taxes.tax_rate')->where('is_deleted', 0);},])->get();
                $alltaxes = Tax::where('is_deleted', 0)
                                ->select(['taxes.id','taxes.name','taxes.tax_rate'])
                                ->orderBy('id','ASC')
                                ->get();
                return array("allgroup" => $allgroup, "alltaxes" => $alltaxes);
            break;

            case 'customer':
                $matchThese = ['is_deleted' => 0, 'domain_id' => Auth::user()->domain_id];
                $allcustomer = Customer::where($matchThese)
                            ->orderBy('id','DESC')
                            ->get();
                return $allcustomer;
            break;

            case 'purchaser':
                $matchThese = ['is_deleted' => 0, 'domain_id' => Auth::user()->domain_id];
                $allpurchaser = Purchaser::where($matchThese)
                            ->orderBy('id','DESC')
                            ->get();
                return $allpurchaser;
            break;
        }
    }
}
