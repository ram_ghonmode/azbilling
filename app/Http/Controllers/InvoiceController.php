<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use PDF;
use App\Domain;
use App\Tax;
use App\Group;
use App\Customer;
use App\Invoice;
use App\Invoice_item;
use App\User;
use App\Workorder;
use App\Workorde_item;

class InvoiceController extends Controller{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function printInvoice($id){
        $allinvoicedata = Invoice::with(['invoice_items' => function($query){$query->with('groups.taxes');},'customers'])->where(['is_deleted' => 0 , 'id' => $id ])->first();
    
        return view('dashboard.print-invoice', compact('allinvoicedata'));
    }

    public function pdfview(Request $request){
        $allinvoicedata = Invoice::with(['invoice_items' => function($query){$query->with('groups.taxes');},'customers'])->where(['is_deleted' => 0 , 'id' => $request->id ])->first();
        view()->share('allinvoicedata',$allinvoicedata);
        if($request->has('download')){
            PDF::setOptions(['dpi' => 72, 'defaultFont' => 'arial']);
            $pdf = PDF::loadView('pdfview');
            return $pdf->download('Invoice'. $allinvoicedata->voucher_no.'.pdf');
        }
        return view('Invoice');
    }

    public function workorderpdfview(Request $request){
        $allworkorderdata = Workorder::with(['workorde_items' => function($query){$query->with('groups.taxes');},'customers'])->where(['is_deleted' => 0 , 'id' => $request->id ])->first();
        view()->share('allworkorderdata',$allworkorderdata);
        if($request->has('download')){
            $pdf = PDF::loadView('workorderpdf');
            return $pdf->download('Workorder'. $allworkorderdata->voucher_no.'.pdf');
        }
        return view('Workorder');
    }

    public function saveinvoice(Request $request){
        // if (isset($_SERVER['HTTP_ORIGIN'])) {
        //     header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        //     header('Access-Control-Allow-Credentials: true');
        //     header('Access-Control-Max-Age: 86400');    // cache for 1 day
        // }
        //   // Access-Control headers are received during OPTIONS requests
        // if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        //    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        //        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

        //    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        //        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        //    exit(0);
        // }
        $result['status'] = false;
        if($request->isMethod('post')){
            $input = $request->all();
            // dd($input); 
            $lastId = Invoice::select('id','voucher_no')->where('domain_id', $input['domain_id'])->orderBy('id', 'DESC')->first();
            if(empty($lastId)){
                $slastId = 1;
            }else{
                $str = $lastId->voucher_no;
                $strArr = explode("-",$str);
                $str_last_val = end($strArr);
                $str_last_num = preg_replace("/[^0-9,.]/", "", $str_last_val);
                $slastId = ($str_last_num + 1);
            }
            $invoice = new Invoice;
            $invoice->customer_id = $input['customer_id'];
            $invoice->domain_id = $input['domain_id'];
            $invoice->voucher_no = $this->invoicevoucheruniqueid($slastId, $input['domain_id']);
            $invoice->invoice_date = isset($input['invoice_date']) ? date('Y-m-d', strtotime($input['invoice_date'])) : null;
            $invoice->total = $input['total'];
            $invoice->advance_amount = isset($input['advance_amount']) ? $input['advance_amount'] : null;
            $invoice->less_amount = isset($input['less']) ? $input['less'] : null;
            $invoice->paytype = $input['payType'];
            // $invoice->tax_price = $input['tax_price'];
            $invoice->status = $input['status'];
            $invoice->grand_total = $input['grand_total'];
            // dd($invoice);
            if($invoice->save()){
                foreach($input['invoice_items'] as $key => $items){
                    $item_records[] = [
                        'description' => $items['description'],
                        'invoice_id' => $invoice->id,
                        'qty' => $items['qty'],
                        // 'tax_price' => $items['tax_price'],
                        // 'taxgroup_id' => $items['taxgroup_id'],
                        'total' => $items['total'],
                        'unit_price' => $items['unit_price']
                    ];
                }
                if(Invoice_item::insert($item_records)){
                    $result['status'] = true;
                    $result['msg'] = 'Data saved';
                    $result['id'] = $invoice->id;
                }else{
                    $result['msg'] = 'Invoice Item data not saved';
                }
            }else{
                $result['msg'] = 'Invoice Data Not Saved';
            }
        }
        // header('Content-Type: application/json');
        return response()->json($result);
    }

    public function updateinvoice($id, Request $request){
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');    // cache for 1 day
        }
          // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
           if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
               header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

           if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
               header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
           exit(0);
        }
        $result['status'] = false;
        if($request->isMethod('post')){
            $input = $request->all();
            $data = array(
                'customer_id' => $input['customer_id'],
                'invoice_date' => isset($input['invoice_date']) ? date('Y-m-d', strtotime($input['invoice_date'])) : null,
                'total' => $input['total'],
                'status' => $input['status'],
                'grand_total' => $input['grand_total'],
                'advance_amount' => $input['advance_amount'],
                'less_amount' => $input['less'],
            );
            if(Invoice::where('id', $id)->update($data)){
                if(Invoice_item::where('invoice_id', $id)->delete()){
                    foreach($input['invoice_items'] as $key => $items){
                        $item_records[] = [
                            'description' => $items['description'],
                            'invoice_id' => $id,
                            'qty' => $items['qty'],
                            'total' => $items['total'],
                            'unit_price' => $items['unit_price']
                        ];
                    }
                    if(Invoice_item::insert($item_records)){
                        $result['status'] = true;
                        $result['msg'] = 'Data Updated';
                        $result['id'] = $id;
                    }else{
                        $result['msg'] = 'Invoice Item data not Updated';
                    }
                }else{
                    $result['msg'] = 'Unable to delete Previous Invoice Items';
                }
            }else{
                $result['msg'] = 'Invoice Data Not Updated';
            }
        }
        header('Content-Type: application/json');
        return response()->json($result);
    }

    public function saveworkorder(Request $request){
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');    // cache for 1 day
        }
          // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
           if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
               header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

           if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
               header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
           exit(0);
        }
        $result['status'] = false;
        if($request->isMethod('post')){
            $input = $request->all();
            $lastId = Workorder::select('id', 'voucher_no')->where('domain_id', $input['domain_id'])->orderBy('id', 'DESC')->first();
            if(empty($lastId)){
                $slastId = 1;
            }else{
                $str = $lastId->voucher_no;
                $strArr = explode("-",$str);
                $str_last_val = end($strArr);
                $str_last_num = preg_replace("/[^0-9,.]/", "", $str_last_val);
                $slastId = ($str_last_num + 1);
            }
            $workorder = new Workorder;
            $workorder->customer_id = $input['customer_id'];
            $workorder->domain_id = $input['domain_id'];
            $workorder->voucher_no = $this->workordervoucheruniqueid($slastId, $input['domain_id']);
            $workorder->workorder_date = isset($input['workorder_date']) ? date('Y-m-d', strtotime($input['workorder_date'])) : null;
            $workorder->total = $input['total'];
            $workorder->tax_price = $input['tax_price'];
            $workorder->grand_total = $input['grand_total'];
            if($workorder->save()){
                foreach($input['invoice_items'] as $key => $items){
                    $item_records[] = [
                        'description' => $items['description'],
                        'workorder_id' => $workorder->id,
                        'qty' => $items['qty'],
                        'tax_price' => $items['tax_price'],
                        'taxgroup_id' => $items['taxgroup_id'],
                        'total' => $items['total'],
                        'unit_price' => $items['unit_price']
                    ];
                }
                if(Workorde_item::insert($item_records)){
                    $result['status'] = true;
                    $result['msg'] = 'Data saved';
                    $result['id'] = $workorder->id;
                }else{
                    $result['msg'] = 'Workorder Item data not saved';
                }
            }else{
                $result['msg'] = 'Workorder Data Not Saved';
            }
        }
        header('Content-Type: application/json');
        return response()->json($result);
    }

    public function updateworkorder($id, Request $request){
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');    // cache for 1 day
        }
          // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
           if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
               header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

           if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
               header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
           exit(0);
        }
        $result['status'] = false;
        if($request->isMethod('post')){
            $input = $request->all();
            $data = array(
                'customer_id' => $input['customer_id'],
                'workorder_date' => isset($input['workorder_date']) ? date('Y-m-d', strtotime($input['workorder_date'])) : null,
                'total' => $input['total'],
                'tax_price' => $input['tax_price'],
                'grand_total' => $input['grand_total'],
            );
            if(Workorder::where('id', $id)->update($data)){
                if(Workorde_item::where('workorder_id', $id)->delete()){
                    foreach($input['workorde_items'] as $key => $items){
                        $item_records[] = [
                            'description' => $items['description'],
                            'workorder_id' => $id,
                            'qty' => $items['qty'],
                            'tax_price' => $items['tax_price'],
                            'taxgroup_id' => $items['taxgroup_id'],
                            'total' => $items['total'],
                            'unit_price' => $items['unit_price']
                        ];
                    }
                    if(Workorde_item::insert($item_records)){
                        $result['status'] = true;
                        $result['msg'] = 'Data Updated';
                        $result['id'] = $id;
                    }else{
                        $result['msg'] = 'Workorder Item data not Updated';
                    }
                }else{
                    $result['msg'] = 'Unable to delete Previous Workorder Items';
                }
            }else{
                $result['msg'] = 'Workorder Data Not Updated';
            }
        }
        header('Content-Type: application/json');
        return response()->json($result);
    }

    public function edituser(Request $request){
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');    // cache for 1 day
        }
          // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
           if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
               header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

           if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
               header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
           exit(0);
        }
        $result['status'] = false;
        if($request->isMethod('post')){
            $input = $request->all();
            $pass = !empty($input['pass']) ? base64_decode($input['pass']) : null;
            $cpass = !empty($input['cpass']) ? base64_decode($input['cpass']) : null;
            $apass = !empty($input['apass']) ? base64_decode($input['apass']) : null;
            if(!empty($input['name']) && !empty($input['info'])){
                $data = array(
                    'name' => $input['name'],
                );
                if(User::where('id', $input['userId'])->update($data)){
                    $result['status'] = true;
                    $result['msg'] = 'User Details Successfully Updated..!!';
                }
            }elseif (!empty($input['name']) && !empty($pass) && !empty($cpass) && !empty($apass)) {
                if($input['pass'] == $input['cpass']){
                    $credentials = [
                        'email' => $input['adminEmail'],
                        'password' => $apass
                    ];
                    if(Auth::attempt($credentials)){
                        $data = array(
                            'name' => $input['name'],
                            'password' => bcrypt($pass),
                        );
                        if(User::where('id', $input['userId'])->update($data)){
                            $result['status'] = true;
                            $result['msg'] = 'Password Successfully Changed..!!';
                        }
                    }else{
                        $result['msg'] = 'Please Enter Correct Admin Password';
                    }
                }else{
                    $result['msg'] = 'Password and Confirm Password Not Matched';
                }
                // $result['admin_Id'] = Auth::user()->id;
            }else{
                $result['msg'] = 'Please Enter All Data For Password Change';
            }
        }
        header('Content-Type: application/json');
        return response()->json($result);
    }

    protected function invoicevoucheruniqueid($id, $companyId){
        $companyData = Domain::find($companyId);
        $initialLabel = $companyData->invoice_prefix;
        $alfabet = "A";
        $currentyear = date('y');
        $cmnth = date('m');
        $invYearChange = date('Y-m-d', strtotime(date('Y').'-03-31'));
        if($cmnth > 3){
            $invExists = Invoice::whereDate('created_at', '>', $invYearChange)->where(['domain_id' => $companyId , 'is_deleted' => 0])->exists();
            if(!$invExists){
                $id = 1;
            }
        }else{
            $ye = date('Y');
            $invYearChange = date('Y-m-d', strtotime(($ye - 1).'-03-31'));
            $invExists = Invoice::whereDate('created_at', '>', $invYearChange)->where(['domain_id' => $companyId , 'is_deleted' => 0])->exists();
            if(!$invExists){
                $id = 1;
            }
        }
        if($cmnth <= 3){
            $fyear = ($currentyear - 1) .' - ' . $currentyear;
        }else{
            $fyear = $currentyear .' - ' . ($currentyear + 1);
        }
        $invunivoucherId = $initialLabel . $fyear . '-' . $alfabet . sprintf("%'.04d\n", $id);
        $invunivoucherId = preg_replace('/\s+/', '', $invunivoucherId);
        return $invunivoucherId;
    }

    protected function workordervoucheruniqueid($id, $companyId){
        $companyData = Domain::find($companyId);
        $initialLabel = $companyData->invoice_prefix;
        $alfabet = "A";
        $currentyear = date('y');
        $cmnth = date('m');
        $invYearChange = date('Y-m-d', strtotime(date('Y').'-03-31'));
        if($cmnth > 3){
            $invExists = Workorder::whereDate('created_at', '>', $invYearChange)->where(['domain_id' => $companyId , 'is_deleted' => 0])->exists();
            if(!$invExists){
                $id = 1;
            }
        }else{
            $ye = date('Y');
            $invYearChange = date('Y-m-d', strtotime(($ye - 1).'-03-31'));
            $invExists = Invoice::whereDate('created_at', '>', $invYearChange)->where(['domain_id' => $companyId , 'is_deleted' => 0])->exists();
            if(!$invExists){
                $id = 1;
            }
        }
        if($cmnth <= 3){
            $fyear = ($currentyear - 1) .' - ' . $currentyear;
        }else{
            $fyear = $currentyear .' - ' . ($currentyear + 1);
        }
        $invunivoucherId = $initialLabel .'WD'. $fyear . '-' . $alfabet . sprintf("%'.04d\n", $id);
        $invunivoucherId = preg_replace('/\s+/', '', $invunivoucherId);
        return $invunivoucherId;
    }
}
