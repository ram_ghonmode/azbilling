<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice_item extends Model
{
    protected $table = 'invoice_items';

    public function invoices(){
		return $this->belongsTo('App\Invoice', 'invoice_id');
	}

	public function groups(){
		return $this->belongsTo('App\Group', 'taxgroup_id');
	}
}
