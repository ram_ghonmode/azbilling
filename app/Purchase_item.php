<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase_item extends Model
{
    protected $table = 'purchase_items';

    public function purchasers(){
		return $this->belongsTo('App\Purchaser', 'purchaser_id');
    }

    public function domains(){
        return $this->belongsTo(Domain::class);
    }
}
