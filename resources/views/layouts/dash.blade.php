<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'INVOICE') }}</title>
    <!-- Bootstrap core CSS-->
    <link href="{{ asset('public/admin_panel/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="{{ asset('public/admin_panel/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="{{ asset('public/admin_panel/vendor/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet">
    <link href="https://cdn.datatables.net/fixedcolumns/3.2.4/css/fixedColumns.bootstrap4.min.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="{{ asset('public/admin_panel/css/sb-admin.css') }}" rel="stylesheet">
    <link href="{{ asset('public/admin_panel/css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/invoice.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/gh/atatanasov/gijgo@1.8.0/dist/combined/css/gijgo.min.css" rel="stylesheet" type="text/css" />


</head>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
    <style>
        .bg-dark,#mainNav.fixed-top.navbar-dark .sidenav-toggler {
            background-color: #0288D1!important;
        }
        #mainNav.navbar-dark .navbar-collapse .navbar-sidenav{
            background: #03A9F5;
        }

        #mainNav.navbar-dark .navbar-collapse .navbar-sidenav > .nav-item .sidenav-second-level,
        #mainNav.navbar-dark .navbar-collapse .navbar-sidenav > .nav-item .sidenav-third-level {
            background: #4fb9f3;
        }
        #mainNav.navbar-dark .navbar-collapse .navbar-sidenav > .nav-item > .nav-link,
        #mainNav.fixed-top.navbar-dark .sidenav-toggler a i,
        #mainNav.navbar-dark .navbar-collapse .navbar-sidenav .nav-link-collapse:after,
        #mainNav.navbar-dark .navbar-collapse .navbar-sidenav > .nav-item .sidenav-second-level > li > a,
        #mainNav.navbar-dark .navbar-collapse .navbar-sidenav > .nav-item .sidenav-third-level > li > a {
            color: #ffffff;
        }

        #mainNav.navbar-dark .navbar-collapse .navbar-sidenav > .nav-item > .nav-link:hover,
        #mainNav.navbar-dark .navbar-collapse .navbar-sidenav > .nav-item .sidenav-second-level > li > a:focus,
        #mainNav.navbar-dark .navbar-collapse .navbar-sidenav > .nav-item .sidenav-second-level > li > a:hover,
        #mainNav.navbar-dark .navbar-collapse .navbar-sidenav > .nav-item .sidenav-third-level > li > a:focus,
        #mainNav.navbar-dark .navbar-collapse .navbar-sidenav > .nav-item .sidenav-third-level > li > a:hover {
            color: #e2f0fd;
        }
        #mainNav.navbar-dark .navbar-collapse .navbar-sidenav li.active a{
            background-color: #1c6486;
        }

        .btn-primary.theme {
            color: #fff;
            background-color: #1c6486;
            border-color: #1c6486;
        }
        .navbar-dark .navbar-nav .nav-link {
            color: rgba(255, 255, 255, 1);
        }
        footer.sticky-footer{
            background-color: #03a9f5;
            color: #f1f1f1;
        }
    </style>
    @include('include.navbar')
    <div class="content-wrapper">
        @yield('content')
        @include('include.footer')
        <!-- /.content-wrapper-->
        <?php
            $action = app('request')->route()->getAction();
            $controller = class_basename($action['controller']);
        ?>
        <!-- Bootstrap core JavaScript-->
        <!--<script src="{{ asset('public/admin_panel/vendor/jquery/jquery.min.js') }}"></script>-->
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script src="{{ asset('public/admin_panel/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <!-- Core plugin JavaScript-->
        @if(Route::currentRouteName() == 'salesreport' || Route::currentRouteName() == 'purchasereport' || Route::currentRouteName() == 'purchasereport.submit')
            <script>
                $(document).ready(function(){
                    $("#reportAnchorId").trigger('click');
                });
            </script>
        @endif

        @if(Route::currentRouteName() == 'invoice' || Route::currentRouteName() == 'invoice_list')
            <script>
                $(document).ready(function(){
                    $("#invoiceAnchorId").trigger('click');
                });
            </script>
        @endif

        @if(Route::currentRouteName() == 'workorder_list' || Route::currentRouteName() == 'workorder')
            <script>
                $(document).ready(function(){
                    $("#workorderAnchorId").trigger('click');
                });
            </script>
        @endif
        <script src="{{ asset('public/admin_panel/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
        <script src="{{ asset('public/admin_panel/vendor/datatables/jquery.dataTables.js') }}"></script>
        <script src="{{ asset('public/admin_panel/vendor/datatables/dataTables.bootstrap4.js') }}"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
        @if(Route::currentRouteName() == 'salesreport' || Route::currentRouteName() == 'purchasereport' || Route::currentRouteName() == 'purchasereport.submit')
            <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap4.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
            <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
        @endif
        <!-- Custom scripts for all pages-->
        <script src="{{ asset('public/admin_panel/js/sb-admin.min.js') }}"></script>

        <!-- Custom scripts for this page-->
        <script src="{{ asset('public/admin_panel/js/sb-admin-datatables.min.js') }}"></script>

        <script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.4/js/dataTables.fixedColumns.min.js"></script>

        <!--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
        <script src="{{ asset('public/js/common.js') }}"></script>
        <script src="{{ asset('public/admin_panel/js/sb-admin-datatables.js') }}"></script>

        <script src="https://cdn.jsdelivr.net/gh/atatanasov/gijgo@1.8.0/dist/combined/js/gijgo.min.js" type="text/javascript"></script>



        <?php if($controller == 'HomeController@invoice'){ ?>
            <script src="{{ asset('public/js/custom.js') }}"></script>
        <?php } ?>

        <?php
        if($controller == 'HomeController@editinvoice'){
        ?>
            <script src="{{ asset('public/js/editinvoice.js') }}"></script>
        <?php
            }
        ?>

        <?php
        if($controller == 'HomeController@workorder'){
        ?>
            <script src="{{ asset('public/js/workorder.js') }}"></script>
        <?php
            }
        ?>

        <?php
        if($controller == 'HomeController@editworkorder'){
        ?>
            <script src="{{ asset('public/js/editworkorder.js') }}"></script>
        <?php
            }
        ?>
    </div>
</body>
</html>