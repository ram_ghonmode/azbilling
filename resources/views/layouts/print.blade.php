<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="{{ asset('public/admin_panel/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
        
    </head>
    <body>
        <div class="content-wrapper">
            @yield('content')
        </div>
        <!-- Jquery Core Js -->
         <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <!-- Bootstrap Core Js -->
        <script src="{{ asset('public/admin_panel/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script>
            $(document).ready(function(){
                window.print();
            });
        </script>
    </body>
</html>