@extends('layouts.dash')

@section('content')
<style>
    .btn-sm{padding: .18rem .4rem;}
</style>
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Invoice List</li>
    </ol>
    @if (isset($success))
        <div class="alert alert-success" onclick="$(this).hide()">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Success !!!</strong> {{ $success }}
        </div>
    @endif
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> List Of Invoices
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered nowrap" id="listInvodataTable" width="100%" cellspacing="0">
                    <thead class="thead-light">
                        <tr>
                            <th>S.No.</th>
                            <th>Invoice No.</th>
                            <th>Date</th>
                            <th>Amount</th>
                            <th>Status</th>
                            <th>Contact</th>
                            <th>Customer Name</th>
                            <th>Company Name</th>
                            <th class="text-center">Action_List</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($allinvoicelist->all() as $key => $invoicelist)
                            <tr>
                                <td class="text-center">{{{ ++$key }}}</td>
                                <td>{{ $invoicelist->voucher_no }}</td>
                                <td>{{ \Carbon\Carbon::parse($invoicelist->invoice_date)->format('d/m/Y')}}</td>
                                <td>{{ $invoicelist->grand_total }}</td>
                                <td class="text-center"><span class="badge @if($invoicelist->status == 'Pending') badge-danger @elseif($invoicelist->status == 'Paid') badge-success @endif ">{{ $invoicelist->status }}</span></td>
                                <td>{{ $invoicelist->customers['contact'] }}</td>
                                <td>{{ $invoicelist->customers['customer_name'] }}</td>
                                <td>{{ $invoicelist->customers['client_name'] }}</td>
                                <td class="text-center" style="">
                                    <a href='{{ url("getinvoice/{$invoicelist->id}") }}' title="View">
                                        <button type="button" class="btn btn-primary btn-sm">
                                            <i class="fa fa-eye"></i>
                                        </button>
                                    </a>
                                    <a href='{{ url("editinvoice/{$invoicelist->id}") }}' title="Edit">
                                        <button type="button" class="btn btn-warning btn-sm">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                    </a>
                                    <a href="{{ url('delete/'.$invoicelist->id.'/Invoice') }}" onclick="return confirm('Are you sure?')" title="Delete">
                                        <button type="button" class="btn btn-danger btn-sm">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{--  <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>  --}}
    </div>

</div>

@endsection
<!-- /.container-fluid-->