@extends('layouts.dash')

@section('content')

<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Workorder List</li>
    </ol>
    @if (isset($success))
        <div class="alert alert-success" onclick="$(this).hide()">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Success !!!</strong> {{ $success }}
        </div>
    @endif
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> List Of Invoices
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered nowrap" id="listInvodataTable" width="100%" cellspacing="0">
                    <thead class="thead-light">
                        <tr>
                            <th>#</th>
                            <th>WorkOrder No.</th>
                             <th>Date</th>
                            <th>Amount</th>
                            <th>Contact</th>
                            <th>Customer Name</th>
                            <th>Company Name</th>
                            <th class="text-center">Actions_List</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($allworkorderlist->all() as $key => $workorderlist)
                            <tr>
                                <td>{{{ ++$key }}}</td>
                                <td>{{ $workorderlist->voucher_no }}</td>
                                <td class="text-center">{{ !empty($workorderlist->workorder_date) ? \Carbon\Carbon::parse($workorderlist->workorder_date)->format('d/m/Y') : 'NA' }}</td>
                                <td>{{ $workorderlist->grand_total }}</td>
                                <td>{{ $workorderlist->customers['contact'] }}</td>
                                <td>{{ $workorderlist->customers['customer_name'] }}</td>
                                <td>{{ $workorderlist->customers['client_name'] }}</td>
                                <td class="text-center">
                                    <a href='{{ url("getworkorder/{$workorderlist->id}") }}' title="View">
                                        <button type="button" class="btn btn-primary btn-sm">
                                            <i class="fa fa-eye"></i>
                                        </button>
                                    </a>
                                    <a href='{{ url("editworkorder/{$workorderlist->id}") }}' title="Edit">
                                        <button type="button" class="btn btn-warning btn-sm">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                    </a>
                                    <a href="{{ url('delete/'.$workorderlist->id.'/Workorder') }}" onclick="return confirm('Are you sure?')" title="Delete">
                                        <button type="button" class="btn btn-danger btn-sm">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{--  <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>  --}}
    </div>

</div>

@endsection
<!-- /.container-fluid-->