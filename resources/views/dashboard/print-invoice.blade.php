@extends('layouts.print')

@section('content')
<div class="container-fluid">
	<style type="text/css">
		table{margin: 0;padding:0;font-size: 13px;}
		.bor-bot{border-bottom: 1px solid #eee;}
		h1,h2,h3,h4,h5,h6{margin: 0;padding:0;font-weight: 600;}
		h1{color: orange; font-weight: 300;}
		h5{font-size:16px;}
		h6{font-size:14px;}
		p{margin:0;padding:0;}
		.mar-bot-5{margin-bottom: 5px;}
		.mar-bot-10{margin-bottom: 10px;}
		.mar-bot-20{margin-bottom: 20px;}
		.mar-bot-30{margin-bottom: 30px;}
		.mar-top-10{margin-top: 10px;}
		.mar-top-20{margin-top: 20px;}
		.mar-top-30{margin-top: 5px;}
		.clr-org{color: #ff580f;}
		.bg-org{background: #ff580f;}
		.clr-wht{color: #fff;}
		.email-pad{padding: 5px;}
	</style>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<table border="0" width="100%" style="font-family: arial;margin:0 auto;" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<table border="0" width="100%" cellpadding="0">
							<tr valign="top">
								<td width="450px">
								<h1 class="mar-bot-10 clr-org" style="font-size: 24px;">
								<!-- <img src='{{ $allinvoicedata->domains->logo2 }}' style="width: 35%;"> -->
								
							</td>
							<td></td>
							<td rowspan="2" width="400px" align="center">
								<img src='{{ $allinvoicedata->domains->logo }}' style="width: 100%;">
								<p class="mar-bot-10"><strong>{{ $allinvoicedata->domains->address }}</p></strong>
							</td>
							</tr>
						</table>
						<table border="0" width="100%" cellpadding="0">
							<tr>
								<!-- <td><b>Landline: </b> {{ $allinvoicedata->domains->landline }}</td> -->
								<td><b>Mob No: </b>{{ $allinvoicedata->domains->mobile }} / {{ $allinvoicedata->domains->mobile2 }}</td>
								<td class="email-pad"><b>Email: </b>{{ $allinvoicedata->domains->email }} </td>
								<td><b>Website: </b>{{ $allinvoicedata->domains->website }} </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table border="0" width="100%" cellspacing="0" cellpadding="3" class="mar-bot-10" style="margin-top:5px;border-top:1px solid #000; border-bottom:1px solid #000; table-layout: fixed;">
							<tr valign="top">
								<td>
									<h6 class="clr-org mar-bot-5">Memo No.</h6>
									{{ $allinvoicedata->voucher_no }}
								</td>
								<td rowspan="2">
									<h6 class="clr-org mar-bot-5">To</h6>
									<h6>{{ $allinvoicedata->customers->client_name }}</h6>
									<p>{{ $allinvoicedata->customers->address }}</p>
									<p>{{ $allinvoicedata->customers->contact }}</p>

								</td>
								<!-- <td rowspan="2" align="left">
									<h6 class="clr-org mar-bot-5">Customer Tax Details</h6>
									<u>
										@if($allinvoicedata->customers->gstinno != null && !empty($allinvoicedata->customers->gstinno))
										<b>GSTIN: <span>{{ $allinvoicedata->customers->gstinno }}</span></b>
										@endif
									</u>
								</td> -->
							</tr>
							<tr>
								<td>
									<h6 class="clr-org mar-top-10 mar-bot-5
									20">Date</h6>
									<?php echo date('M d, Y',  strtotime($allinvoicedata->invoice_date)); ?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table  border="0" width="100%" cellpadding="3" cellspacing="0">
							<tr align="left" bgcolor="#ff580f" class="clr-wht">
								<th width="10%">Sr.No.</th>
								<th width="30%">Description</th>
								<th>Qty</th>
								<th>Unit Price</th>
								<th>Total(Rs.)</th>
							</tr>
							<?php $TotalWithOutTax = 0; ?>
							@foreach ($allinvoicedata->invoice_items as $key => $items)
							<?php $TotalWithOutTax = $items->total - $items->tax_price; ?>
								<tr align="left">
									<td class="bor-bot">{{{ ++$key }}}</td>
									<td class="bor-bot">{{ $items->description }}</td>
									<td class="bor-bot">{{ $items->qty }}</td>
									<td class="bor-bot"><?php echo number_format((float)$items->unit_price, 2) ?></td>
									<td class="bor-bot"><?php echo number_format((float)$TotalWithOutTax, 2) ?> /-</td>
								</tr>
							@endforeach

							<tr align="left">
								<td colspan="5" height="10px"></td>
							</tr>

							<tr bgcolor="#ffefe2">
								<td colspan="3"></td>
								<td>Subtotal</td>
								<td><?php echo number_format((float)$allinvoicedata->total, 2) ?> /-</td>
							</tr>
							<?php $taxAmt = $StaxAmt = 0; $TaxesAppliedArr = []; ?>
							
							@foreach ($TaxesAppliedArr as $tkey => $tvalues)
								<tr bgcolor="#ffefe2">
									<td colspan="3"></td>
									<td><?php echo number_format((float)$tvalues, 2) ?>  /-</td>
								</tr>
							@endforeach
							<tr bgcolor="#ffefe2">
								<td colspan="3"></td>
								<td>
								@if(!empty($allinvoicedata->less_amount))
									Less
								@endif
								</td>
								<td>
								@if(!empty($allinvoicedata->less_amount))
									<?php echo number_format((float)$allinvoicedata->less_amount, 2) ?> /-
								@endif
								</td>
							</tr>
							<tr bgcolor="#ffefe2">
								<td colspan="3"></td>
								<td>Advance Paid</td>
								<td><?php echo number_format((float)$allinvoicedata->advance_amount, 2) ?> /-</td>
							</tr>
							<?php $grandTot;  $grandTot = $allinvoicedata->total-$allinvoicedata->less_amount-$allinvoicedata->advance_amount?>
							<tr class="bg-org">
								<td colspan="3"class="clr-wht"><b></b></td>
								<td><b class="clr-wht">Total to Pay</b></td>
								<td><b class="clr-wht"><?php echo number_format((float)$grandTot, 2) ?> /-</b></td>
							</tr>
						</table>
					</td>
				</tr>
				
				<tr>
					<td align="right">
						<p class="clr-org mar-top-20">Mode of payment {{ $allinvoicedata->paytype }}.</p>
						<p class="clr-org mar-top-20">Thanks for choosing {{ $allinvoicedata->domains->name }}.</p>
					</td>
				</tr>

				<tr>
					<td>
						<p class="mar-top-30">
							<i><b class="clr-org">Payment:</b> We accept cash, online transfer, Cheque and credit cards.</i>
						</p>
					</td>
				</tr>
				<tr>
					<td>
						
					</td>
				</tr>
				<tr>
				<tr>
					<td>
						<table width="100%" cellpadding="10" cellspacing="0" class="mar-top-30" style="font-size:12px;border-top:1px solid #eee;">
							<tr>
								<td class="email-pad"><b>Email: </b>{{ $allinvoicedata->domains->email }} </td>
								<td style={padding:5px;} align="right"><b>Website: </b>{{ $allinvoicedata->domains->website }} </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="10" cellspacing="0" class="mar-top-30" style="font-size:10px;border-top:1px solid #eee;">
							<tr>
								<td class="email-pad"><b>Bank A/c: </b>{{ $allinvoicedata->domains->bankaccno }}
								       <b style="margin-left: 5px" >Bank Name: </b>{{ $allinvoicedata->domains->bankname }}        <b style="margin-left: 5px" >IFSC: </b>{{ $allinvoicedata->domains->ifsc }} </td>
							</tr>
						</table>
					</td>
				</tr>
					<td>
						<table width="100%" cellpadding="10" cellspacing="0" class="mar-top-30" style="font-size:12px;border-top:1px solid #eee;">
							<tr>
								<td class="text-center"><h5>We are the <strong><u>Change</u></strong></h5></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
@endsection
