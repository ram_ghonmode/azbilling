@extends('layouts.dash')

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Sales Report</li>
    </ol>
    <style>
        .table-bordered thead td, .table-bordered thead th{
            vertical-align: middle;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> List Of Sales Report
        </div>
        <div class="row card-body">
            <div class="table-responsive">
                <table class="table table-bordered nowrap" id="saledataTable" width="100%" cellspacing="0">
                    <thead class="thead-light">
                        <tr>
                            <th rowspan="2" class="text-center">S.No.</th>
                            <th rowspan="2" class="text-center">GSTIN/UIN of Recipient</th>
                            <th rowspan="2" class="text-center">Invoice Number</th>
                            <th rowspan="2" class="text-center">Invoice date</th>
                            <th rowspan="2" class="text-center">Invoice Value (Incl. Gst)</th>
                            <th rowspan="2" class="text-center">Rate</th>
                            <th rowspan="2" class="text-center">Taxable Value</th>
                            <th class="text-center" colspan="3">GST</th>
                        </tr>
                        <tr>
                            <th>CGST</th>
                            <th>SGST</th>
                            <th>IGST</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $cgsttot = 0; $sgsttot = 0; $igsttot = 0; ?>
                        @foreach ($allinvoices as $key => $invoice)
                            <?php $taxName = $taxRate = $taxNameF = $cgst = $sgst = $igst = null; $gst = 0; ?>
                            @foreach ($invoice->invoice_items as $invKey => $items)
                                <?php
                                    $taxName = substr($items->groups->name, 0, 3);
                                    $taxNameF = preg_replace('/\s+/', '', $taxName);
                                    $taxRate = substr($items->groups->name, -3);
                                    $gst += $items->tax_price;
                                ?>
                            @endforeach
                            <?php
                                if($taxNameF == 'IN'){
                                    $cgst = number_format((float)$gst/2, 2);
                                    $sgst = number_format((float)$gst/2, 2);
                                }elseif ($taxNameF == 'OUT') {
                                    $igst = $gst;
                                }

                                $cgsttot += (float)$cgst;
                                $sgsttot += (float)$sgst;
                                $igsttot += (float)$igst;
                            ?>
                        <tr class="text-center">
                            <td>{{{ ++$key }}}</td>
                            <td>{{ $invoice->customers->gstinno }}</td>
                            <td>{{ $invoice->voucher_no }}</td>
                            <td>{{ date('d-m-Y', strtotime($invoice->invoice_date)) }}</td>
                            <td>{{ $invoice->grand_total }}</td>
                            <td>{{ $taxRate }}</td>
                            <td>{{ $invoice->total }}</td>
                            <td>{{ $cgst }}</td>
                            <td>{{ $sgst }}</td>
                            <td>{{ $igst }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="2" class="text-right">Total Amount</th>
                            <th colspan="5"></th>
                            <th>{{ $cgsttot }}</th>
                            <th>{{ $sgsttot }}</th>
                            <th>{{ $igsttot }}</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection