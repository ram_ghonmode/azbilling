@extends('layouts.dash')

@section('content')

<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Company Details</li>
    </ol>
    @if (isset($success))
        <div class="alert alert-success" onclick="$(this).hide()">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Success !!!</strong> {{ $success }}
        </div>
    @endif
    <div class="card mb-3" id="">
        <div class="card-body">
            <form  method="POST" action="{{ route('domain.submit') }}" id="" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-row">
                    <input type="hidden" name="oldId" id="domainOldId">
                    <div class="col-4">
                        <label><b>Company Name</b> <sup class="text-danger">*</sup></label>
                        <input type="text" name="name" class="form-control" value="{{ !empty($domaindata->name) ? $domaindata->name : '' }}" placeholder="Company Name">
                        @if ($errors->has('name'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-8">
                        <label for="inputAddress"><b>Address</b> <sup class="text-danger">*</sup></label>
                        <input type="text" name="address" class="form-control" value="{{ !empty($domaindata->address) ? $domaindata->address : '' }}"  id="inputAddress" placeholder="1234 Main St">
                        @if ($errors->has('address'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('address') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row" style="margin: 8px -5px;">
                    <div class="col-4">
                        <label><b> Mobile No. </label></b> <sup class="text-danger">*</sup>
                        <input type="text" name="mobile" class="form-control" value="{{ !empty($domaindata->mobile) ? $domaindata->mobile : '' }}"  placeholder="Mobile Number">
                        @if ($errors->has('mobile'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('mobile') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-4">
                        <label><b> Alternate Mobile No. </label></b>
                        <input type="text" name="mobile2" class="form-control" value="{{ !empty($domaindata->mobile2) ? $domaindata->mobile2 : '' }}"  placeholder="Alternate Mobile Number">
                        @if ($errors->has('mobile2'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('mobile2') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-4">
                        <label><b>Landline No. </label></b>
                        <input type="text" name="landline" class="form-control" value="{{ !empty($domaindata->landline) ? $domaindata->landline : '' }}"  placeholder="Landline Number">
                        @if ($errors->has('landline'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('landline') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row" style="margin: 8px -5px;">
                    <div class="col-4">
                        <label><b>Email</label></b>
                        <input type="email" name="email" class="form-control" value="{{ !empty($domaindata->email) ? $domaindata->email : '' }}"  placeholder="Enter Email">
                        @if ($errors->has('email'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-4">
                        <label><b>Website</label></b>
                        <input type="text" name="website" class="form-control" value="{{ !empty($domaindata->website) ? $domaindata->website : '' }}"  placeholder="Enter Your Website">
                    </div>
                    <div class="col-4">
                        @if(!empty($domaindata->logo))
                                <b><label>Current Logo </label></b><br>
                                <input id="fileInput" type="file" name="logo" style="display:none;" onchange="setNewImageToDisp(this);" />
                                <img src="{{ !empty($domaindata->logo) ? $domaindata->logo : '' }}" width="100px" alt="No Logo">&nbsp;&nbsp;&nbsp;&nbsp;
                                <button type="button" class="btn btn-outline-info btn-sm fa fa-camera" onclick="document.getElementById('fileInput').click();" title="Click to change Logo"></button>&nbsp;&nbsp;&nbsp;&nbsp;
                                <img id="previewImgId" class="d-none" src="" alt="new_selected_img" width="100px">
                                <button id="closeBtnId" type="button" class="close d-none" aria-label="Close" title="Remove Selected Image" onclick="removeSelectedImg();">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        @else
                            <label><b>Logo Image </label> <span class="text-warning">Max Size : 2MB</span></b>
                            <input type="file" name="logo" class="form-control" style="padding: 3px;">
                            @if ($errors->has('logo'))
                                <span class="help-block text-danger">
                                    <strong class="text-danger">{{ $errors->first('logo') }}</strong>
                                </span>
                            @endif
                        @endif
                    </div>
                </div>
                <div style="border-bottom: 1px solid; margin: 18px 0px;"></div>
                <div class="form-row">
                    <input type="hidden" name="oldId" id="domainOldId">
                    <div class="col-3">
                        <label><b>Customer Prefix</b> <sup class="text-danger">*</sup> <small>(for customer series)</small></label>
                        <input type="text" name="customer_prefix" class="form-control" value="{{ !empty($domaindata->customer_prefix) ? $domaindata->customer_prefix : '' }}"  placeholder="Example. CS or CUS">
                        @if ($errors->has('customer_prefix'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('customer_prefix') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-3">
                        <label for="inputAddress"><b>Invoice Prefix</b> <sup class="text-danger">*</sup> <small>(for invoice series)</small></label>
                        <input type="text" name="invoice_prefix" class="form-control" id="inputAddress" value="{{ !empty($domaindata->invoice_prefix) ? $domaindata->invoice_prefix : '' }}"  placeholder="Example. IN or INV">
                        @if ($errors->has('invoice_prefix'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('invoice_prefix') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-3">
                        <label><b>GSTIN No. </label></b>
                        <input type="text" name="gstinno" class="form-control" value="{{ !empty($domaindata->gstinno) ? $domaindata->gstinno : '' }}"  placeholder="GSTIN Number">
                    </div>
                    <div class="col-3">
                        <label><b>PAN No. </label></b>
                        <input type="text" name="pan_no" class="form-control" value="{{ !empty($domaindata->pan_no) ? $domaindata->pan_no : '' }}"  placeholder="PAN Number">
                    </div>
                </div>
                <div class="form-row" style="margin: 8px -5px;">
                    <div class="col-3">
                        <label><b>Bank Account No. </label></b>
                        <input type="text" name="bankaccno" class="form-control" value="{{ !empty($domaindata->bankaccno) ? $domaindata->bankaccno : '' }}"  placeholder="Bank Account Number">
                    </div>
                    <div class="col-3">
                        <label><b>Bank Name </label></b>
                        <input type="text" name="bankname" class="form-control" value="{{ !empty($domaindata->bankname) ? $domaindata->bankname : '' }}"  placeholder="Bank Name">
                    </div>
                    <div class="col-3">
                        <label><b>Bank IFSC Code. </label></b>
                        <input type="text" name="ifsc" class="form-control" value="{{ !empty($domaindata->ifsc) ? $domaindata->ifsc : '' }}"  placeholder="Bank IFSC Code">
                    </div>
                    <div class="col-1"></div>
                    <div class="col-2">
                        <label>&nbsp;</label>
                        <input type="submit" class="form-control btn btn-success" value="{{ !empty($domaindata->name) ? 'Update' : 'Save' }}" style="cursor:pointer;">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
<!-- /.container-fluid-->