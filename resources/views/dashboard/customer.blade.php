@extends('layouts.dash')

@section('content')

<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Customer Details</li>
        <li class="breadcrumb-item"><button type="button" class="btn btn-primary btn-sm addbutton" onclick="showDomainRefFormDiv();">Add Customer</button></li>
    </ol>
    @if (isset($success))
        <div class="alert alert-success" onclick="$(this).hide()">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Success !!!</strong> {{ $success }}
        </div>
    @endif
    <div class="card mb-3" id="domainRefrenceFormDivId" style="@if(empty($errors->first('name'))) display: none; @endif">
        <div class="card-header">
            <i class="fa fa-life-ring"></i> Add New Customer Details
        </div>
        <div class="card-body">
            <form  method="POST" action="{{ route('customer.submit') }}" id="" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-row">
                    <input type="hidden" name="customer_id" id="domainOldId">
                    <div class="col-4">
                        <label><b>Customer Name</b> <sup class="text-danger">*</sup></label>
                        <input type="text" name="client_name" class="form-control" placeholder="Customer Name" required>
                        @if ($errors->has('client_name'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('client_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-4">
                        <label><b>Contact <sup class="text-danger">*</sup></label></b>
                        <input type="number" name="contact" class="form-control" placeholder="Contact" required>
                        @if ($errors->has('contact'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('contact') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-4">
                        <label><b>Reference Name</label></b>
                        <input type="text" name="customer_name" class="form-control" placeholder="Reference Name">
                        @if ($errors->has('customer_name'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('customer_name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row" style="margin: 8px -5px;">
                    <div class="col-7">
                        <label for="inputAddress"><b>Address</b></label>
                        <input type="text" name="address" class="form-control" id="inputAddress" placeholder="1234 Main St" required>
                    </div>
                    <div class="col-5">
                        <label><b>Email</label></b>
                        <input type="email" name="email" class="form-control" placeholder="Email">
                        @if ($errors->has('email'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-3">
                        <label><b>GSTIN No. </label></b>
                        <input type="text" name="gstinno" class="form-control" placeholder="GSTIN Number">
                        @if ($errors->has('gstinno'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('gstinno') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-7"></div>
                    <div class="col-2">
                        <label>&nbsp;</label>
                        <input type="submit" class="form-control btn btn-success" value="Save" style="cursor:pointer;">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> List Of Registered Customers
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered nowrap" id="cusdataTable" width="100%" cellspacing="0">
                    <thead class="thead-light">
                        <tr>
                            <th>S.No.</th>
                            <th>Client No.</th>
                            <th>Customer Name</th>
                            <th>GSTIN No.</th>
                            <th>Reference Name</th>
                            <th>Contact</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($customer as $key => $cust)
                        <tr>
                            <td class="text-center">{{{ ++$key }}}</td>
                            <td>{{ $cust->client_no }}</td>
                            <td>{{ $cust->client_name }}</td>
                            <td>{{ $cust->gstinno }}</td>
                            <td>{{ $cust->customer_name }}</td>
                            <td>{{ $cust->contact }}</td>
                            <td>{{ $cust->email }}</td>
                            <td>{{ $cust->address }}</td>
                            <td class="text-center">
                                <a href='{{ url("editcustomer/{$cust->id}") }}' title="Edit">
                                    <button type="button" class="btn btn-info btn-sm">
                                        <i class="fa fa-pencil"></i>
                                    </button>
                                </a>
                                <a href="{{ url('delete/'.$cust->id.'/Customer') }}" onclick="return confirm('Are you sure?')" title="Delete">
                                    <button type="button" class="btn btn-danger btn-sm">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{--  <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>  --}}
    </div>
</div>
@endsection
<!-- /.container-fluid-->