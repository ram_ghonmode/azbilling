@extends('layouts.dash')

@section('content')

<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Tax Group</li>
        <li class="breadcrumb-item"><button type="button" class="btn btn-primary btn-sm addbutton" onclick="showTaxGroupAddFormDiv();">Add Tax Group</button></li>
    </ol>
    @if (isset($success))
        <div class="alert alert-success" onclick="$(this).hide()">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Success !!!</strong> {{ $success }}
        </div>
    @endif
    <div class="card mb-3" id="TaxAddGroupFormDivId" style="@if(empty($errors->any())) display: none; @endif">
        <div class="card-header">
            <i class="fa fa-life-ring"></i> Add New Tax Group
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route('taxgroup.submit') }}">
                {{ csrf_field() }}
                <div class="form-row">
                    <div class="col-4">
                        <label><b>Tax Group Name</b> <sup class="text-danger">*</sup></label>
                        <input type="text" name="name" class="form-control" placeholder="Enter Tax Group Name" required>
                        @if ($errors->has('name'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-8">
                        <label><b>Description</b></label>
                        <input type="text" name="description" class="form-control" placeholder="Enter Description of Group">
                        @if ($errors->has('description'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-10" style="margin-top:8px;">
                        <label style="display:block;margin-bottom:0;"><b class="text-danger">Select Tax Under Group</b> <sup class="text-danger">*</sup></label>
                        @foreach ($alltaxesngroup['alltaxes'] as $key => $tax)
                        <div class="form-check form-check-inline col-2">
                            <label class="form-check-label text-info" style="margin-right: 12px;">
                                <input class="form-check-input" name="tax[]" style="margin-top: 6px;" type="checkbox" value="{{ $tax->id }}"> <strong>{{ $tax->name }}</strong>
                            </label>
                        </div>
                        @endforeach
                    </div>
                    <div class="col-2">
                        <label>&nbsp;</label>
                        <input type="submit" class="form-control btn btn-success" value="Save" style="cursor:pointer;">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> List Of Registered Tax Group
        </div>
        <div class="row card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead class="thead-light">
                        <tr>
                            <th class="text-center">S.No.</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Tax Assigned</th>
                            <th class="text-center">Description</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th class="text-center">S.No.</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Tax Assigned</th>
                            <th class="text-center">Description</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($alltaxesngroup['allgroup'] as $gkey => $group)
                            <tr class="text-center">
                                <td>{{{ ++$gkey }}}</td>
                                <td>{{ $group->name }}</td>
                                <td>
                                    @if(isset($group['taxes']))
                                        @foreach ($group['taxes'] as $gtkey => $gtax)
                                            {{ $gtax->name }} {{', '}}
                                        @endforeach
                                    @endif
                                </td>
                                <td>{{ $group->description }}</td>
                                <td class="text-center">
                                    <a href="{{ url('edittaxgroup/'.$group->id) }}">
                                        <button type="button" class="btn btn-warning btn-xs" title="Edit" style="padding: 0 6px; cursor:pointer;" onclick=""><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
<!-- /.container-fluid-->