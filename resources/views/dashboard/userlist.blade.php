@extends('layouts.dash')

@section('content')

<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('register') }}">Add User</a>
        </li>
        <li class="breadcrumb-item active">User List</li>
    </ol>
    @if (isset($success))
        <div class="alert alert-success" onclick="$(this).hide()">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Success !!!</strong> {{ $success }}
        </div>
    @endif
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> List Of Registered Users / Admins
        </div>
        <div class="card-body row">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead class="thead-light">
                        <tr>
                            <th>S.No.</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>S.No.</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach($alluser as $key => $user)
                        <tr>
                            <td>{{{ ++$key }}}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>
                            	@if($user->role != 2)
                                <span class="badge badge-danger" title="Admin">Admin</span>
                                @else
                                    <span class="badge badge-success" title="User">User</span>
                                @endif
                            </td>
                            <td class="text-center">
                                <button type="button" class="btn btn-warning btn-xs" title="Edit" style="padding: 0 6px;" onclick="openUserEditModal({{ $user }});"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{--  <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>  --}}
    </div>

</div>

<div class="modal fade" id="userEditModalCenter" tabindex="-1" role="dialog" aria-labelledby="userEditModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit User <span class="text-info"></span> Information</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="alertDivId">
                </div>
                <form>
                    <div class="form-group row">
                        <label for="nameId" class="col-sm-3 col-form-label">Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="nameId" placeholder="Name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="eUserEmailId" class="col-sm-3 col-form-label">Email</label>
                        <div class="col-sm-9">
                            <input type="email" class="form-control" id="eUserEmailId" placeholder="Email" disabled="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="eUserroleId" class="col-sm-3 col-form-label">Role</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="eUserroleId" placeholder="Role" disabled="">
                        </div>
                    </div>

                    <div id="changePasswordDivId" class="card" style="display:none;">
                        <div class="card-header">
                            Change Password
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="eNewPassId" class="col-sm-4 col-form-label">New Password</label>
                                <div class="col-sm-8">
                                    <input type="password" class="form-control" id="eNewPassId" placeholder="Password">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="eConfPassId" class="col-sm-4 col-form-label">Conf. Password</label>
                                <div class="col-sm-8">
                                    <input type="password" class="form-control" id="eConfPassId" placeholder="Password">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="eAdminPassId" class="col-sm-4 col-form-label text-danger">Admin Password</label>
                                <div class="col-sm-8">
                                    <input type="password" class="form-control" id="eAdminPassId" placeholder="Password">
                                    <input type="hidden" value="{{ Auth::user()->email }}" id="eAdminEmailId">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer" style="display:unset;">
                <div class="row">
                    <div class="col-sm-3">
                        <button type="button" class="btn btn-link btn-sm float-left" onclick="showChangePassDiv();">Change Password</button>
                    </div>
                    <div class="col-sm-9 text-right">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary btn-sm" onclick="saveEditUserInfo();">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
<!-- /.container-fluid-->