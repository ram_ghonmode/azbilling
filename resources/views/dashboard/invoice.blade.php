@extends('layouts.dash')

@section('content')

<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Create Invoice</li>
        <li class="breadcrumb-item"><button type="button" class="btn btn-primary btn-sm addbutton" data-toggle="modal" data-target="#myModal">Add Customer</button></li>
    </ol>
    @if (isset($success))
        <div class="alert alert-success" onclick="$(this).hide()">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Success !!!</strong> {{ $success }}
        </div>
    @endif
    <div class="card mb-3" id="">
        <div class="card-header">
            <i class="fa fa-life-ring"></i> Create Invoice
        </div>
        <div class="card-body">
                {{ csrf_field() }}
                <div class="row">
                	<div class="col-12">
	                	<div class="row justify-content-between customer-head">
							<div class="col-6">
								<b style="font-size: 18px;">Company Name: <span>{{ $alldomain->name }}</span></b>
                                <input type="hidden"  id="user_id" value="{{ $alldomain->id }}">
							</div>
							<div class="col-6">
								<b style="font-size: 18px;" class="pull-right">GSTIN NO : <span>{{ $alldomain->gstinno }}</span></b>
							</div>
						</div>
                	</div>
            	</div>
                <div class="form-row justify-content-between">
                    <div class="col-4">
                        <label><b>Select Customer <sup class="text-danger">*</sup></label></b>
                        <select class="form-control" name="customer_name" id="selectcustId" required>
                                <option></option>
                            @foreach ($allcustomer as $key => $customer)
                                <option value="{{ $customer->id }}" data-indexval="{{{ $key }}}">{{ $customer->client_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <input type="hidden" name="domain_id" id="domainId" value="{{ Auth::user()->domain_id }}">
                    <div class="col-4">
                        <label><b>Date</b> <sup class="text-danger">*</sup></label>
                        <input id="invoice_dateId" class="form-control gj-datepicker" value="{{date('d-m-Y')}}">
                        @if ($errors->has('invoice_date'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('invoice_date') }}</strong>
                            </span>
                        @endif
                    </div>
                </div><br>
                <div class="form-row" style="min-height: 40px;">
                    <div class="col-4" style="min-height: 40px;">
                        <label><b>Customer Address</b></label>
                    </div>
                    <div class="col-4" style="min-height: 40px;">
                        <label><b>Customer GSTIN No</label></b>
                    </div>
                    <div class="col-4" style="min-height: 40px;">
                        <p class="pull-right" style="margin: 0;"><button type="button" class="btn btn-primary btn-xs addbutton addmore" id="additen">Add Fields</button></p>
                    </div>
                </div>
                <div class="form-row" id="custdetailId" style="display: none;">
                    <div class="col-4">
                        <p id="CustaddId"></p>
                    </div>
                    <div class="col-4">
                        <p id="CustgstnId"></p>
                    </div>
                </div>
                <div class="form-row">
					<table class="table table-bordered invoice-table" id="splashTable">
						<thead style="background-color: rgba(0,0,0,.03);">
					        <th>#</th>
					        <th style="width: 45%;">Description</th>
					        <th style="width: 10%;">Unit</th>
                            <th style="width: 12%;">Unit Price</th>
					        <th>Total</th>
					        <th></th>
					    </thead>
                        <tfoot>
                            <tr>
                                <td colspan="3"></td>
                                <td class="text-center">
                                    <b>Less</b>
                                </td>
                                <td class="text-center">
                                    <input type="text" id="lessAmtId" value="" class="form-control"> 
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="2"></td>
                                <td>
                                    <select name="paytype" id="payId" class="form-control bg-success text-light" onchange="checkForInvoicePay(this);">
                                        <option value="Cash">Cash</option>
                                        <option value="Card">Card</option>
                                        <option value="Cheque">Cheque</option>
                                        <option value="Online">Online</option>
                                    </select>
                                </td>
                                <td class="text-center">
                                    <b>Amount Received</b>
                                </td>
                                <td class="text-center">
                                    <input type="text" id="advanceAmtId" value="" class="form-control"> 
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="3"></td>
                                <td class="text-center">
                                    <b><span id="subtotalId"></span></b>
                                </td>
                                <td colspan="2" class="text-center">
                                    <b><span id="gtotalId"></span></b>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3"></td>
                                <td class="text-center">
                                    <b>Sub-Total</b>
                                </td>
                                <td colspan="2" class="text-center">
                                    <b>Grand Total</b>
                                </td>
                            </tr>
                        </tfoot>
					    <tbody>

						</tbody>
				    </table>
                    <div class="clearfix">
                        <div class="float-left" style="margin-right:10px;">
                            <select name="status" id="statusId" class="form-control bg-danger text-light" onchange="checkForInvoiceStatus(this);">
                                <option value="Pending">Pending</option>
                                <option value="Paid">Paid</option>
                            </select>
                        </div>
                        <!-- <div class="float-left" style="margin-right:10px;">
                            <select name="paytype" id="payId" class="form-control bg-success text-light" onchange="checkForInvoicePay(this);">
                                <option value="Cash">Cash</option>
                                <option value="Card">Card</option>
                                <option value="Cheque">Cheque</option>
                                <option value="Online">Online</option>
                            </select>
                        </div> -->
                        <div class="float-left">
                            <input type="button" class="btn btn-primary btn-xs" id="submitInvoiceId" value="Submit" style="cursor:pointer;">
                        </div>
                    </div>
               </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add New Customer Details</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <form  method="POST" action="{{ route('customer.submit') }}" id="" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-row">
                            <input type="hidden" name="customer_id" id="domainOldId">
                            <div class="col-4">
                                <label><b>Customer Name</b> <sup class="text-danger">*</sup></label>
                                <input type="text" name="client_name" class="form-control" placeholder="Customer Name" required>
                                @if ($errors->has('client_name'))
                                    <span class="help-block text-danger">
                                        <strong class="text-danger">{{ $errors->first('client_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-4">
                                <label><b>Contact <sup class="text-danger">*</sup></label></b>
                                <input type="number" name="contact" class="form-control" placeholder="Contact" required>
                                @if ($errors->has('contact'))
                                    <span class="help-block text-danger">
                                        <strong class="text-danger">{{ $errors->first('contact') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-4">
                                <label><b>Reference Name</label></b>
                                <input type="text" name="customer_name" class="form-control" placeholder="Reference Name">
                                @if ($errors->has('customer_name'))
                                    <span class="help-block text-danger">
                                        <strong class="text-danger">{{ $errors->first('customer_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-row" style="margin: 8px -5px;">
                            <div class="col-7">
                                <label for="inputAddress"><b>Address</b></label>
                                <input type="text" name="address" class="form-control" id="inputAddress" placeholder="1234 Main St" required>
                            </div>
                            <div class="col-5">
                                <label><b>Email</label></b>
                                <input type="email" name="email" class="form-control" placeholder="Email">
                                @if ($errors->has('email'))
                                    <span class="help-block text-danger">
                                        <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-3">
                                <label><b>GSTIN No. </label></b>
                                <input type="text" name="gstinno" class="form-control" placeholder="GSTIN Number">
                                @if ($errors->has('gstinno'))
                                    <span class="help-block text-danger">
                                        <strong class="text-danger">{{ $errors->first('gstinno') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-7"></div>
                            <div class="col-2">
                                <label>&nbsp;</label>
                                <input type="submit" class="form-control btn btn-success" value="Save" style="cursor:pointer;">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    CustomerObj = null; CustomerjsonToObj = null;
    CustomerObj = '<?php echo !empty($allcustomer) ? json_encode($allcustomer) : null ?>';
    CustomerjsonToObj = JSON.parse(CustomerObj);

    TaxObj = null; TaxjsonToObj = null;
    TaxObj = '<?php echo !empty($alltaxes) ? json_encode($alltaxes) : null ?>';
    TaxjsonToObj = JSON.parse(TaxObj);
    TotalTaxArr = [];

</script>
@endsection