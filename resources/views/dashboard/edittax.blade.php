@extends('layouts.dash')

@section('content')

<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('taxes') }}">All Taxes</a>
        </li>
        <li class="breadcrumb-item active">Edit Tax Record</li>
    </ol>
    @if (isset($success))
        <div class="alert alert-success" onclick="$(this).hide()">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Success !!!</strong> {{ $success }}
        </div>
    @endif
     <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-life-ring"></i> Edit Tax
        </div>
        <div class="card-body">
            <form method="POST" action="{{ url('edittax/'.$tax->id) }}">
                {{ csrf_field() }}
                <div class="form-row">
                    <div class="col-3">
                        <label><b>Tax Name</b> <sup class="text-danger">*</sup></label>
                        <input type="text" name="name" value="{{ $tax->name }}" class="form-control" placeholder="Enter Tax Name" required>
                        @if ($errors->has('name'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>	
                    <div class="col-3">
                        <label><b>Tax Rate ( % )</b> <sup class="text-danger">*</sup></label>
                        <input type="number" name="tax_rate" value="{{ $tax->tax_rate }}" class="form-control" placeholder="Enter tax_rate" required>
                        @if ($errors->has('tax_rate'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('tax_rate') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-3">
                        <label><b>Tax Number</b></label>
                        <input type="text" name="tax_number" value="{{ $tax->tax_number }}" class="form-control" placeholder="Enter tax_number">
                        @if ($errors->has('tax_number'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('tax_number') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-3">
                        <label><b>Effective From (DD-MM-YYYY)</b></label>
                        <input type="text" name="effective_from" class="form-control" value="{{ isset($tax->effective_from) ? date('d-m-Y', strtotime($tax->effective_from)) : null }}" placeholder="Date DD-MM-YYYY">
                        @if ($errors->has('effective_from'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('effective_from') }}</strong>
                            </span>
                        @endif
                    </div>                    
                </div>
                <div class="form-row" style="margin: 8px -5px;">
                    <div class="col-3">
                        <label><b>Effective Till (DD-MM-YYYY)</b></label>
                        <input type="text" name="effective_till" value="{{ isset($tax->effective_till) ? date('d-m-Y', strtotime($tax->effective_till)) : null }}" class="form-control"  placeholder="Date DD-MM-YYYY">
                        @if ($errors->has('effective_till'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('effective_till') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-7">
                        <label><b>Description</b></label>
                        <input type="text" name="description" value="{{ $tax->description }}" class="form-control" placeholder="Enter Description of Tax">
                        @if ($errors->has('description'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-2">
                        <label>&nbsp;</label>
                        <input type="submit" class="form-control btn btn-success" value="Update" style="cursor:pointer;">
                    </div> 
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
<!-- /.container-fluid-->