@extends('layouts.dash')

@section('content')
<div class="container-fluid" style="padding: 40px;">
    <div class="row headertable">
        <div class="col">
            <h3>Webakruti</h3>
            <address>Plot No. 11,Madhav Nagar, Behind Domino’s Pizza,
               Near Mate Square, Nagpur – 440010</address>
            <span>Workorder {{ $allworkorderdata->voucher_no }}</span>
        </div>
        <div class="col">
            <img src="{{ $allworkorderdata->domains->logo }}" class="pull-right logo"><br>
        </div>
    </div>
    <div class="row" style="margin-top: 5px;">
        <div class="col">
            <strong>Date</strong>
        </div>
        <div class="col">
            <strong>To</strong>
        </div>
        <div class="col">

        </div>
    </div>
    <div class="row headertable">
        <div class="col">
            <input id="invoice_dateId" class="form-control gj-datepicker" name="workorder_date"  value="<?php echo date('M d,Y',  strtotime($allworkorderdata->workorder_date)); ?>">
            <strong>Customer ID</strong><br>
            <span id="CuststId"></span>
        </div>
        <div class="col">
            <select class="form-control" id="selectcustId">
                @foreach ($allcustomer as $key => $customers)
                    <option value="{{ $allworkorderdata->customers->id }}" data-indexval="{{{ $key }}}" {{ ( $customers->id == $allworkorderdata->customers->id ) ? 'selected' : '' }}>{{ $customers->client_name }}</option>
                @endforeach
            </select>
            <address id="CustaddId">{{ $allworkorderdata->customers->address }}</address>
        </div>
        <div class="col">

        </div>
    </div>
    <div class="form-row addfieldbtn">
        <p><button type="button" class="btn btn-primary addrow">Add Fields</button></p>
    </div>
    <div class="form-row" style="clear: both;">
        <table class="table table-bordered" id="splashTable">
            <thead>
                <th>#</th>
                <th>Description</th>
                <th>Unit</th>
                <th>Unit Price</th>
                <th>Taxes</th>
                <th>Total</th>
                <th></th>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="3"></td>
                    <td class="text-center">
                        <b><span id="subtotalId"></span></b>
                    </td>
                    <td class="text-center">
                        <b><span id="taxtotalId"></span></b>
                    </td>
                    <td colspan="2" class="text-center">
                        <b><span id="gtotalId"></span></b>
                    </td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                    <td class="text-center">
                        <b>Sub-Total</b>
                    </td>
                    <td class="text-center">
                        <b>Tax Total</b>
                    </td>
                    <td colspan="2" class="text-center">
                        <b>Grand Total</b>
                    </td>
                </tr>
            </tfoot>
        <tbody id="tabCount">
            <?php $i = 1;?>
            <input type="hidden" value="{{ $allworkorderdata->id }}" id="editWorkorderId">
            @foreach($allworkorderdata->workorde_items  as $key => $workorder)
            <tr>
                <td>{{{ ++$key }}}</td>
                <td><input class="form-control" type="text" id="desc_{{ $i }}" value="{{ $workorder->description }}"></td>
                <td><input class="form-control qty key" type="number" id="qty_{{ $i }}" value="{{ $workorder->qty }}"></td>
                <td><input class="form-control price key" type="number" id="price_{{ $i }}" value="{{ $workorder->unit_price }}"></td>
                <td>
                    <select class="form-control" onchange="findTaxesFromTaxGroup(this)">
                        <option value='0'></option>
                        @foreach($alltaxes as $keys => $taxes)
                            <option value="{{ $taxes->id }}" data-taxes="{{{ $keys }}}" {{ ( $taxes->id == $workorder->taxgroup_id ) ? 'selected' : '' }}>{{ $taxes->name }}</option>
                        @endforeach
                    </select>
                </td>
                <td><input class="form-control" type="number" id="item_total_{{ $i }}" value="{{ $workorder->total }}" readonly></td>
                <td><button type='button' class='btn btn-danger deleteBtn'><i class='fa fa-times' aria-hidden='true'></i></button></td>
            </tr>
            <?php $i++; ?>
            @endforeach
        </tbody>
        </table>
        <div class="form-row">
            <input type="button" class="btn btn-primary btn-xs" id="submitWorkorderId" value="Update" style="cursor:pointer;">
        </div>
    </div>
</div>
@endsection
<script type="text/javascript">
    CustomerObj = null; CustomerjsonToObj = null;
    CustomerObj = '<?php echo !empty($allcustomer) ? json_encode($allcustomer) : null ?>';
    CustomerjsonToObj = JSON.parse(CustomerObj);

    TaxObj = null; TaxjsonToObj = null;
    TaxObj = '<?php echo !empty($alltaxes) ? json_encode($alltaxes) : null ?>';
    TaxjsonToObj = JSON.parse(TaxObj);
    TotalTaxArr = [];
</script>