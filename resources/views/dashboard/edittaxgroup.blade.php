@extends('layouts.dash')

@section('content')

<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('taxgroup') }}">All Group Tax</a>
        </li>
        <li class="breadcrumb-item active">Edit Group Tax</li>
    </ol>
    @if (isset($success))
        <div class="alert alert-success" onclick="$(this).hide()">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Success !!!</strong> {{ $success }}
        </div>
    @endif
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-life-ring"></i> Edit Tax Group
        </div>
        <div class="card-body">
            <form method="POST" action="{{ url('edittaxgroup/'.$taxgroup->id) }}">             
                {{ csrf_field() }}
                <div class="form-row">
                    <div class="col-4">
                        <label><b>Tax Group Name</b> <sup class="text-danger">*</sup></label>
                        <input type="text" name="name" value="{{ $taxgroup->name }}" class="form-control" placeholder="Enter Tax Group Name" required>
                        @if ($errors->has('name'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-8">
                        <label><b>Description</b></label>
                        <input type="text" name="description" value="{{ $taxgroup->description }}" class="form-control" placeholder="Enter Description of Group">
                        @if ($errors->has('description'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-10" style="margin-top:8px;">
                        <label style="display:block;margin-bottom:0;"><b class="text-danger">Select Tax Under Group</b> <sup class="text-danger">*</sup></label>
                        @foreach ($alltax as $key => $tax)                               
                        <div class="form-check form-check-inline col-2">
                            <label class="form-check-label text-info" style="margin-right: 12px;">
                                <input class="form-check-input" name="tax[]" style="margin-top: 6px;" type="checkbox" value="{{ $tax->id }}" @foreach ($taxgroup['taxes'] as $tkey => $tgroup) @if($tgroup->id == $tax->id) checked @endif @endforeach> <strong>{{ $tax->name }}</strong>
                            </label>
                        </div>                        
                        @endforeach 
                    </div> 
                    <div class="col-2">
                        <label>&nbsp;</label>
                        <input type="submit" class="form-control btn btn-success" value="Update" style="cursor:pointer;">
                    </div> 
                </div>
            </form>
        </div>
    </div>
</div>

@endsection