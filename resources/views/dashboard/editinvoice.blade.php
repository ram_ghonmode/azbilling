@extends('layouts.dash')

@section('content')
<div class="container-fluid" style="padding: 40px;">
    <div class="row headertable">
        <div class="col">
            <h3>RadhaVilas</h3>
            <address>351, Great Nag Road, Nagpur – 440009</address>
            <!-- <span>Invoice WS17-18-A0011</span> -->
        </div>
        <div class="col">
            <img src="{{ $allinvoicedata->domains->logo }}" class="pull-right logo"><br>
            <strong class="pull-right" style="clear: both;"></strong>
        </div>
    </div>
    <div class="row" style="margin-top: 5px;">
        <div class="col">
            <strong>Date</strong>
        </div>
        <div class="col">
            <strong>To</strong>
        </div>
        <!-- <div class="col">
            <strong>Customer Tax Details</strong>
        </div> -->
    </div>
    <div class="row headertable">
        <div class="col">
            <input id="invoice_dateId" class="form-control gj-datepicker" name="invoice_date"  value="<?php echo date('M d,Y',  strtotime($allinvoicedata->invoice_date)); ?>">
            <strong>Customer ID</strong><br>
            <span id="CuststId"></span>
        </div>
        <div class="col">
            <select class="form-control" id="selectcustId">
                @foreach ($allcustomer as $key => $customers)
                    <option value="{{ $allinvoicedata->customers->id }}" data-indexval="{{{ $key }}}" {{ ( $customers->id == $allinvoicedata->customers->id ) ? 'selected' : '' }}>{{ $customers->client_name }}</option>
                @endforeach
            </select>
            <address id="CustaddId">{{ $allinvoicedata->customers->address }}</address>
        </div>
        <div class="col">

        </div>
    </div>
    <div class="form-row addfieldbtn">
        <p><button type="button" class="btn btn-primary addrow">Add Fields</button></p>
    </div>
    <div class="form-row" style="clear: both;">
        <table class="table table-bordered" id="splashTable">
            <thead>
                <th>#</th>
                <th>Description</th>
                <th>Unit</th>
                <th>Unit Price</th>
                <th>Total</th>
                <th></th>
            </thead>
            <tfoot>
            <tr>
                    @if(!empty($allinvoicedata->less_amount))
                        <td colspan="3"></td>
                        <td class="text-center">
                            <b>Less</b>
                        </td>
                        <td class="text-center">
                            <input type="text" id="lessAmtId" class="form-control" value="{{ $allinvoicedata->less_amount ?? '' }}"> 
                        </td>
                        <td></td>
                    @endif    
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td>
                        <select name="paytype" id="payId" class="form-control bg-success text-light" onchange="checkForInvoicePay(this);">
                            <option value="Cash">Cash</option>
                            <option value="Card">Card</option>
                            <option value="Cheque">Cheque</option>
                            <option value="Online">Online</option>
                        </select>
                    </td>
                    <td class="text-center">
                        <b>Amount Received</b>
                    </td>
                    <td class="text-center">
                        <input type="text" id="advanceAmtId" class="form-control" value="{{ $allinvoicedata->advance_amount ?? '' }}"> 
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                    <td class="text-center">
                        <b><span id="subtotalId"></span></b>
                    </td>
                    <td colspan="2" class="text-center">
                        <b><span id="gtotalId"></span></b>
                    </td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                    <td class="text-center">
                        <b>Sub-Total</b>
                    </td>
                    <td colspan="2" class="text-center">
                        <b>Grand Total</b>
                    </td>
                </tr>
            </tfoot>
        <tbody id="tabCount">
            <?php $i = 1;?>
            <input type="hidden" value="{{ $allinvoicedata->id }}" id="editInvoiceId">
            @foreach($allinvoicedata->invoice_items  as $key => $invoice)
            <tr>
                <td>{{{ ++$key }}}</td>
                <td><input class="form-control" type="text" id="desc_{{ $i }}" value="{{ $invoice->description }}"></td>
                <td><input class="form-control qty key" type="number" id="qty_{{ $i }}" value="{{ $invoice->qty }}"></td>
                <td><input class="form-control price key" type="number" id="price_{{ $i }}" value="{{ $invoice->unit_price }}"></td>
                <td><input class="form-control" type="number" id="item_total_{{ $i }}" value="{{ $invoice->total }}" readonly></td>
                <td><button type='button' class='btn btn-danger deleteBtn'><i class='fa fa-times' aria-hidden='true'></i></button></td>
            </tr>
            <?php $i++; ?>
            @endforeach
        </tbody>
        </table>
        <div class="clearfix">
            <div class="float-left" style="margin-right:10px;">
                <select name="status" id="statusId" class="form-control bg-danger text-light" onchange="checkForInvoiceStatus(this);">
                    <option value="Pending" @if($allinvoicedata->status == 'Pending') selected @endif>Pending</option>
                    <option value="Paid" @if($allinvoicedata->status == 'Paid') selected @endif >Paid</option>
                </select>
            </div>
            <div class="float-left">
                <input type="button" class="btn btn-primary btn-xs" id="submitInvoiceId" value="Update" style="cursor:pointer;">
            </div>
        </div>
    </div>
</div>
@endsection
<script type="text/javascript">
    CustomerObj = null; CustomerjsonToObj = null;
    CustomerObj = '<?php echo !empty($allcustomer) ? json_encode($allcustomer) : null ?>';
    CustomerjsonToObj = JSON.parse(CustomerObj);

    TaxObj = null; TaxjsonToObj = null;
    TaxObj = '<?php echo !empty($alltaxes) ? json_encode($alltaxes) : null ?>';
    TaxjsonToObj = JSON.parse(TaxObj);
    TotalTaxArr = [];
</script>