@extends('layouts.dash')

@section('content')

<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Edit Customer</li>
    </ol>
    @if (isset($success))
        <div class="alert alert-success" onclick="$(this).hide()">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Success !!!</strong> {{ $success }}
        </div>
    @endif
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-life-ring"></i> Edit Customer Details
        </div>
        <div class="card-body">
            <form  method="POST" action="{{ url('editcustomer/'.$customer->id) }}" id="" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-row">
                    <input type="hidden" name="customer_id" id="domainOldId">
                    <div class="col-4">
                        <label><b>Client/Organization Name</b> <sup class="text-danger">*</sup></label>
                        <input type="text" name="client_name" class="form-control" value="{{ $customer->client_name }}">
                        @if ($errors->has('client_name'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('client_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-4">
                        <label><b>Contact <sup class="text-danger">*</sup></label></b>
                        <input type="number" name="contact" class="form-control" value="{{ $customer->contact }}">
                        @if ($errors->has('contact'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('contact') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-4">
                        <label><b>Customer Name</label></b>
                        <input type="text" name="customer_name" class="form-control" value="{{ $customer->customer_name }}">
                        @if ($errors->has('customer_name'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('customer_name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row" style="margin: 8px -5px;">
                    <div class="col-7">
                        <label for="inputAddress"><b>Address</b></label>
                        <input type="text" name="address" class="form-control" id="inputAddress" value="{{ $customer->address }}">
                    </div>
                    <div class="col-5">
                        <label><b>Email</label></b>
                        <input type="email" name="email" class="form-control" value="{{ $customer->email }}">
                        @if ($errors->has('email'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-3">
                        <label><b>GSTIN No. </label></b>
                        <input type="text" name="gstinno" class="form-control" value="{{ $customer->gstinno }}">
                        @if ($errors->has('gstinno'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('gstinno') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-7"></div>
                    <div class="col-2">
                        <label>&nbsp;</label>
                        <input type="submit" class="form-control btn btn-success" value="Update" style="cursor:pointer;">
                    </div>
                </div>
            </form>
        </div>
    </div>    
</div>

@endsection
<!-- /.container-fluid-->