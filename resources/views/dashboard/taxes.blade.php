@extends('layouts.dash')

@section('content')

<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Tax Record</li>
        <li class="breadcrumb-item"><button type="button" class="btn btn-primary btn-sm addbutton" onclick="showTaxAddFormDiv();">Add Tax</button></li>
    </ol>
    @if (isset($success))
        <div class="alert alert-success" onclick="$(this).hide()">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Success !!!</strong> {{ $success }}
        </div>
    @endif
    <div class="card mb-3" id="TaxAddFormDivId" style="@if(empty($errors->any())) display: none; @endif">
        <div class="card-header">
            <i class="fa fa-life-ring"></i> Add New Tax
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route('taxes.submit') }}" id="">
                {{ csrf_field() }}
                <div class="form-row">
                    <div class="col-3">
                        <label><b>Tax Name</b> <sup class="text-danger">*</sup></label>
                        <input type="text" name="name" class="form-control" placeholder="Enter Tax Name" required>
                        @if ($errors->has('name'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-3">
                        <label><b>Tax Rate ( % )</b> <sup class="text-danger">*</sup></label>
                        <input type="number" name="tax_rate" class="form-control" placeholder="Enter tax_rate" required>
                        @if ($errors->has('tax_rate'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('tax_rate') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-3">
                        <label><b>Tax Number</b></label>
                        <input type="text" name="tax_number" class="form-control" placeholder="Enter tax_number">
                        @if ($errors->has('tax_number'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('tax_number') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-3">
                        <label><b>Effective From (DD-MM-YYYY)</b></label>
                        <input type="text" name="effective_from" class="form-control" value="{{ date('d-m-Y') }}" placeholder="Date DD-MM-YYYY">
                        @if ($errors->has('effective_from'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('effective_from') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row" style="margin: 8px -5px;">
                    <div class="col-3">
                        <label><b>Effective Till (DD-MM-YYYY)</b></label>
                        <input type="text" name="effective_till" class="form-control"  placeholder="Date DD-MM-YYYY">
                        @if ($errors->has('effective_till'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('effective_till') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-7">
                        <label><b>Description</b></label>
                        <input type="text" name="description" class="form-control" placeholder="Enter Description of Tax">
                        @if ($errors->has('description'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-2">
                        <label>&nbsp;</label>
                        <input type="submit" class="form-control btn btn-success" value="Save" style="cursor:pointer;">
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> List Of Registered Tax
        </div>
        <div class="row card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead class="thead-light">
                        <tr>
                            <th class="text-center">S.No.</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Tax Rate ( % )</th>
                            <th class="text-center">Effective From</th>
                            <th class="text-center">Effective Till</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th class="text-center">S.No.</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Tax Rate ( % )</th>
                            <th class="text-center">Effective From</th>
                            <th class="text-center">Effective Till</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($taxes as $key => $tax)
                        <tr class="text-center">
                            <td>{{{ ++$key }}}</td>
                            <td>{{ $tax->name }}</td>
                            <td>{{ $tax->tax_rate }}</td>
                            <td>{{ !empty($tax->effective_from) ? date('d F, Y', strtotime($tax->effective_from)) : null }}</td>
                            <td>{{ !empty($tax->effective_till) ? date('d F, Y', strtotime($tax->effective_till)) : null }}</td>
                            <td class="text-center">
                                <a href="{{ url('edittax/'.$tax->id) }}">
                                    <button type="button" class="btn btn-warning btn-xs" title="Edit" style="padding: 0 6px; cursor:pointer;"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
<!-- /.container-fluid-->