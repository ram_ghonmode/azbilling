@extends('layouts.dash')

@section('content')
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Purchase Report</li>
        <li class="breadcrumb-item"><button type="button" class="btn btn-primary btn-sm addbutton" onclick="showDomainRefFormDiv();"><i class="fa fa-plus"></i> Purchase Entry</button></li>
    </ol>
    @if (isset($success))
        <div class="alert alert-success" onclick="$(this).hide()">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Success !!!</strong> {{ $success }}
        </div>
    @endif
    <style>
        .table-bordered thead td, .table-bordered thead th{
            vertical-align: middle;
        }
    </style>
    <div class="card mb-3" id="domainRefrenceFormDivId" style="@if(empty($errors->first('purchaser_id'))) display: none; @endif">
        <div class="card-header">
            <i class="fa fa-life-ring"></i> Add New Purchase Entry
        </div>
        <div class="card-body">
            <form  method="POST" action="{{ route('purchasereport.submit') }}">
                {{ csrf_field() }}
                <div class="form-row">
                    <input type="hidden" name="purchaseitemId" id="oldPurchaseItemId">
                    <div class="col-3">
                        <label><b>Select Vendor <sup class="text-danger">*</sup></label></b><br>
                        <select class="form-control" name="purchaser_id" id="selectPurcId" required>
                            <option></option>
                            @foreach ($allpurchasers as $key => $purchaser)
                                <option value="{{ $purchaser->id }}"> {{ $purchaser->vendor_name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('purchaser_id'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('purchaser_id') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-3">
                        <label><b>Invoice Date</label></b>
                        <input type="text" name="invoice_date" class="form-control gj-datepicker" placeholder="Invoice Date" id="invoice_dateId" value="{{ date('d-m-Y') }}">
                        @if ($errors->has('invoice_date'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('invoice_date') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-3">
                        <label><b>Invoice No. <sup class="text-danger">*</sup></label></b>
                        <input type="text" name="invoice_no" class="form-control" placeholder="Invoice Number" required>
                        @if ($errors->has('invoice_no'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('invoice_no') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-3">
                        <label><b>Invoice Value. <sup class="text-danger">*</sup></label></b>
                        <input type="text" name="invoice_value" class="form-control" placeholder="Invoice Value" required>
                        @if ($errors->has('invoice_value'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('invoice_value') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row" style="margin: 8px -5px;">
                    <div class="col-1">
                        <label><b>Tax_Rate</b></label>
                        <input type="text" name="rate" class="form-control" placeholder="Rate">
                        @if ($errors->has('rate'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('rate') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-3">
                        <label><b>Taxable Value</b></label>
                        <input type="text" name="taxable_value" class="form-control" placeholder="Taxable Value">
                        @if ($errors->has('taxable_value'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('taxable_value') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-8">
                        <div class="form-row">
                            <div class="col-4">
                                <label><b>CGST</b></label>
                                <input type="text" name="cgst" class="form-control" placeholder="CGST">
                                @if ($errors->has('cgst'))
                                    <span class="help-block text-danger">
                                        <strong class="text-danger">{{ $errors->first('cgst') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-4">
                                <label><b>SGST</b></label>
                                <input type="text" name="sgst" class="form-control" placeholder="SGST">
                                @if ($errors->has('sgst'))
                                    <span class="help-block text-danger">
                                        <strong class="text-danger">{{ $errors->first('sgst') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-4">
                                <label><b>IGST</b></label>
                                <input type="text" name="igst" class="form-control" placeholder="IGST">
                                @if ($errors->has('igst'))
                                    <span class="help-block text-danger">
                                        <strong class="text-danger">{{ $errors->first('igst') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <label><b>Description</label></b>
                    <div class="col-12">
                        <textarea name="description" class="form-control" id="descpId" rows="2"></textarea>
                        @if ($errors->has('description'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row" style="margin: 8px -5px;">
                    <label><b>Remark</label></b>
                    <div class="col-12">
                        <textarea name="remark" class="form-control" id="remarkId" rows="1"></textarea>
                        @if ($errors->has('remark'))
                            <span class="help-block text-danger">
                                <strong class="text-danger">{{ $errors->first('remark') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-8"></div>
                    <div class="col-2">
                        <label>&nbsp;</label>
                        <input type="button" class="form-control btn btn-danger d-none" value="Cancel" style="cursor:pointer;" id="editCancelBtnId" onclick="closeEditPurchaseEntry();" title="Cancel">
                    </div>
                    <div class="col-2">
                        <label>&nbsp;</label>
                        <input type="submit" class="form-control btn btn-success" value="Save" style="cursor:pointer;">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> List Of Purchase Report
        </div>
        <div class="row card-body">
            <div class="table-responsive">
                <table class="table table-bordered nowrap" id="reportDataTable" width="100%" cellspacing="0">
                    <thead class="thead-light">
                        <tr>
                            <th rowspan="2" class="text-center">#</th>
                            <th rowspan="2" class="text-center">GSTIN</th>
                            <th rowspan="2" class="text-center">Invoice_No.</th>
                            <th rowspan="2" class="text-center">Invoice_Date.</th>
                            <th rowspan="2" class="text-center">Invoice Value (Incl. Gst)</th>
                            <th rowspan="2" class="text-center">Rate_(%)</th>
                            <th rowspan="2" class="text-center">Taxable Value</th>
                            <th class="text-center" colspan="3">GST</th>
                            <th rowspan="2" class="text-center">Description</th>
                            <th rowspan="2" class="text-center">Remark</th>
                            <th rowspan="2" class="text-center notinclude">Action_List</th>
                        </tr>
                        <tr>
                            <th>CGST</th>
                            <th>SGST</th>
                            <th>IGST</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $invoiceValTot = 0; $taxValTot = 0; $cgstTot = 0; $sgstTot = 0; $igstTot = 0; @endphp
                        @foreach ($purchaseItems as $key => $item)
                            <tr>
                                <td>{{{ ++$key }}}</td>
                                <td>{{ $item->purchasers->gstinno }}</td>
                                <td>{{ $item->invoice_no }}</td>
                                <td>{{ date('d-m-Y', strtotime($item->invoice_date)) }}</td>
                                <td>{{ $item->invoice_value }}</td>
                                <td>{{ $item->rate }}</td>
                                <td>{{ $item->taxable_value }}</td>
                                <td>{{ $item->cgst }}</td>
                                <td>{{ $item->sgst }}</td>
                                <td>{{ $item->igst }}</td>
                                <td>{{ $item->description }}</td>
                                <td>{{ $item->remark }}</td>
                                <td class="text-center">
                                    <a href="javascript:void(0);" onclick="editPurchaserEntry({{ $item }});" title="Edit">
                                        <button type="button" class="btn btn-info btn-sm">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                    </a>
                                    <a href="{{ url('delete/'.$item->id.'/Purchase_item') }}" onclick="return confirm('Are you sure?')" title="Delete">
                                        <button type="button" class="btn btn-danger btn-sm">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                            @php
                                $invoiceValTot += $item->invoice_value;
                                $taxValTot += $item->taxable_value;
                                $cgstTot += $item->cgst;
                                $sgstTot += $item->sgst;
                                $igstTot += $item->igst;
                            @endphp
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="2"></th>
                            <th colspan="2" class="text-right">Invoice Total</th>
                            <th>{{ $invoiceValTot }}</th>
                            <th>Total</th>
                            <th>{{ $taxValTot }}</th>
                            <th>{{ $cgstTot }}</th>
                            <th>{{ $sgstTot }}</th>
                            <th>{{ $igstTot }}</th>
                            <th colspan="3"></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection