<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="{{ route('home') }}">{{ config('app.name', 'INVOICE') }}</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
            <li class="nav-item @if(Route::currentRouteName() == 'home') active @endif" data-toggle="tooltip" data-placement="right" title="Dashboard">
                <a class="nav-link" href="{{ route('home') }}">
                    <i class="fa fa-fw fa-dashboard"></i>
                    <span class="nav-link-text">Dashboard</span>
                </a>
            </li>
            @if(Auth::user()->role != 1)
            <li class="nav-item @if(Route::currentRouteName() == 'domain') active @endif" data-toggle="tooltip">
                <a class="nav-link" href="{{ route('domain') }}">
                    <i class="fa fa-fw fa-user"></i>
                    <span class="nav-link-text">Profile</span>
                </a>
            </li>
            @endif
            @if(Auth::user()->role != 1 && !empty(Auth::user()->domain_id))
            <li class="nav-item @if(Route::currentRouteName() == 'customer') active @endif" data-toggle="tooltip">
                <a class="nav-link" href="{{ route('customer') }}">
                    <i class="fa fa-users"></i>
                    <span class="nav-link-text">&nbsp;Customers</span>
                </a>
            </li>
            @endif
            @if(Auth::user()->role == 1)
            <!-- <li class="nav-item @if(Route::currentRouteName() == 'userlist') active @endif" data-toggle="tooltip" data-placement="right" title="User List">
                <a class="nav-link" href="{{ route('userlist') }}">
                    <i class="fa fa-list"></i>
                    <span class="nav-link-text">&nbsp;User List</span>
                </a>
            </li> -->
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="All Users">
                <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents3" data-parent="#exampleAccordion">
                    <i class="fa fa-fw fa-user"></i>
                    <span class="nav-link-text">Users</span>
                </a>
                <ul class="sidenav-second-level collapse" id="collapseComponents3">
                    <li class="@if(Route::currentRouteName() == 'register') active @endif">
                        <a href="{{ route('register') }}">Add User</a>
                    </li>
                    <li class="@if(Route::currentRouteName() == 'userlist') active @endif">
                        <a href="{{ route('userlist') }}">User List</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Configuration">
                <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
                    <i class="fa fa-fw fa-wrench"></i>
                    <span class="nav-link-text">Tax Configuration</span>
                </a>
                <ul class="sidenav-second-level collapse" id="collapseComponents">
                    <li class="@if(Route::currentRouteName() == 'taxes') active @endif">
                        <a href="{{ route('taxes') }}">Taxes</a>
                    </li>
                    <li class="@if(Route::currentRouteName() == 'taxgroup') active @endif">
                        <a href="{{ route('taxgroup') }}">Tax Groups</a>
                    </li>
                </ul>
            </li>
            @endif
            @if(Auth::user()->role != 1 && !empty(Auth::user()->domain_id))
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Configuration">
                <a id="invoiceAnchorId" class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents1" data-parent="#exampleAccordion">
                    <i class="fa fa-file-text-o"></i>
                    <span class="nav-link-text">&nbsp;Invoice</span>
                </a>
                <ul class="sidenav-second-level collapse" id="collapseComponents1">
                    <li class="@if(Route::currentRouteName() == 'invoice') active @endif">
                        <a href="{{ route('invoice') }}">Create Invoice</a>
                    </li>
                    <li class="@if(Route::currentRouteName() == 'invoice_list') active @endif">
                        <a href="{{ route('invoice_list') }}">View Invoice</a>
                    </li>
                </ul>
            </li>
            @endif
            <!-- @if(Auth::user()->role != 1 && !empty(Auth::user()->domain_id))
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Configuration">
                <a id="workorderAnchorId" class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents2" data-parent="#exampleAccordion">
                    <i class="fa fa-file-text-o"></i>
                    <span class="nav-link-text">&nbsp;WorkOrder</span>
                </a>
                <ul class="sidenav-second-level collapse" id="collapseComponents2">
                    <li class="@if(Route::currentRouteName() == 'workorder') active @endif">
                        <a href="{{ route('workorder') }}">Create Workorder</a>
                    </li>
                    <li class="@if(Route::currentRouteName() == 'workorder_list') active @endif">
                        <a href="{{ route('workorder_list') }}">View Workorder</a>
                    </li>
                </ul>
            </li>
            @endif -->
            @if(Auth::user()->role != 1 && !empty(Auth::user()->domain_id))
            <li class="nav-item @if(Route::currentRouteName() == 'purchaser' || Route::currentRouteName() == 'purchaser.submit') active @endif" data-toggle="tooltip">
                <a class="nav-link" href="{{ route('purchaser') }}">
                    <i class="fa fa-users"></i>
                    <span class="nav-link-text">&nbsp;Vendors</span>
                </a>
            </li>
            @endif
            <!-- @if(Auth::user()->role != 1 && !empty(Auth::user()->domain_id))
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Configuration">
                <a id="reportAnchorId" class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents3" data-parent="#exampleAccordion">
                    <i class="fa fa-file-text-o"></i>
                    <span class="nav-link-text">&nbsp;Report</span>
                </a>
                <ul class="sidenav-second-level collapse" id="collapseComponents3">
                    <li class="@if(Route::currentRouteName() == 'salesreport') active @endif">
                        <a href="{{ route('salesreport') }}">Sales</a>
                    </li>
                    <li class="@if(Route::currentRouteName() == 'purchasereport') active @endif">
                        <a href="{{ route('purchasereport') }}">Purchase</a>
                    </li>
                </ul>
            </li>
            @endif -->
        </ul>
        <ul class="navbar-nav sidenav-toggler">
            <li class="nav-item">
                <a class="nav-link text-center" id="sidenavToggler">
                    <i class="fa fa-fw fa-angle-left"></i>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <!-- <div class="input-group">
                    <input class="form-control text-primary" type="text" value="" disabled="">
                    <span class="input-group-btn">
                        <button class="btn btn-primary theme" type="button">
                            <i class="fa fa-user"></i>
                        </button>
                    </span>
                </div> -->
            </li>
            <li class="nav-item">
                <a class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fa fa-fw fa-sign-out"></i>Logout
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
    </div>
</nav>