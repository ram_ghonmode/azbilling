<!DOCTYPE html>
<html>
	<head>
		<title>Invoice</title>

		<style type="text/css">

			table{
				margin: 0;
				padding:0;
				font-size: 13px;
			}

			.bor-bot{border-bottom: 1px solid #eee;}

			h1,h2,h3,h4,h5,h6{
				margin: 0;
				padding:0;
				font-weight: 600;
			}

			h1{color: orange; font-weight: 300;}

			h5{font-size:16px;}

			h6{font-size:14px;}

			p{
				margin:0;
				padding:0;
			}

			.mar-bot-5{
				margin-bottom: 5px;
			}

			.mar-bot-10{
				margin-bottom: 10px;
			}

			.mar-bot-20{
				margin-bottom: 20px;
			}

			.mar-bot-30{
				margin-bottom: 30px;
			}

			.mar-top-10{
				margin-top: 10px;
			}

			.mar-top-20{
				margin-top: 20px;
			}

			.mar-top-30{
				margin-top: 30px;
			}

			.clr-org{
				color: #ff580f;
			}

			.bg-org{
				background: #ff580f;
			}

			.clr-wht{
				color: #fff;
			}
		</style>
	</head>

	<body>
		<table border="0" width="100%" style="font-family: arial;" cellpadding="0" cellspacing="0">
			<tr >
				<td>
					<table border="0" width="100%" cellpadding="0">
						<tr valign="top">
							<td width="450px">
								<h1 class="mar-bot-10 clr-org" style="font-size: 24px;">{{ $allworkorderdata->domains->name }}</h1>
								<p class="mar-bot-10">{{ $allworkorderdata->domains->address }}</p>
							</td>
							<td></td>
							<td rowspan="2" width="250px" align="center">
								<img src='{{ $allworkorderdata->domains->logo }}' style="width: 100%;">
							</td>
						</tr>
						<tr>
							<td>
								<h6 class="mar-bot-10"></h6>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table border="0" width="100%" cellspacing="0" cellpadding="5" class="mar-bot-10" style="border-top:1px solid #000; border-bottom:1px solid #000; table-layout: fixed;">
						<tr valign="top">
							<td>
								<h6 class="clr-org mar-bot-5">Workorder No.</h6>
								{{ $allworkorderdata->voucher_no }}
							</td>
							<td rowspan="2">
								<h6 class="clr-org mar-bot-5">To</h6>
								<h6>{{ $allworkorderdata->customers->client_name }}</h6>
								<p>{{ $allworkorderdata->customers->address }}</p>

							</td>
							<td rowspan="2" align="center">
								<h6 class="clr-org mar-bot-5"></h6>

							</td>
						</tr>
						<tr>
							<td>
								<h6 class="clr-org mar-top-10 mar-bot-5
								20">Date</h6>
								<?php echo date('M d, Y',  strtotime($allworkorderdata->workorder_date)); ?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table  border="0" width="100%" cellpadding="7" cellspacing="0">
						<tr align="left" bgcolor="#ff580f" class="clr-wht">
							<th width="50px">Sr.No.</th>
							<th width="340px">Description</th>
							<th>Qty</th>
							<th>Unit Price</th>
							<th>Total(Rs.)</th>
						</tr>
                        <?php $TotalWithOutTax = 0; ?>
            	      	@foreach ($allworkorderdata->workorde_items as $key => $items)
            	      	<?php $TotalWithOutTax = $items->total - $items->tax_price; ?>
    						<tr align="left">
    							<td class="bor-bot">{{{ ++$key }}}</td>
    							<td class="bor-bot">{{ $items->description }}</td>
    							<td class="bor-bot">{{ $items->qty }}</td>
    							<td class="bor-bot"><?php echo number_format((float)$items->unit_price, 2) ?></td>
    							<td class="bor-bot"><?php echo number_format((float)$TotalWithOutTax, 2) ?> /-</td>
    						</tr>
                        @endforeach

						<tr align="left">
							<td colspan="5" height="10px"></td>
						</tr>

						<tr bgcolor="#ffefe2">
							<td colspan="3"></td>
							<td>Subtotal</td>
							<td><?php echo number_format((float)$allworkorderdata->total, 2) ?> /-</td>
						</tr>
                        <?php $taxAmt = $StaxAmt = 0; $TaxesAppliedArr = []; ?>
            		    @foreach ($allworkorderdata->workorde_items as $skey => $sitems)
            		    	<?php $taxAmt = $sitems->total - $sitems->tax_price; ?>
            		    	@foreach ($sitems->groups->taxes as $tkey => $taxes)
            		    		<?php
            		    			$StaxAmt = $taxAmt * $taxes->tax_rate / 100;
            		    			isset($TaxesAppliedArr[$taxes->name]) ? $TaxesAppliedArr[$taxes->name] += $StaxAmt : $TaxesAppliedArr[$taxes->name] = $StaxAmt;
            	    			?>
            		      @endforeach
            		    @endforeach
            		    @foreach ($TaxesAppliedArr as $tkey => $tvalues)
    						<tr bgcolor="#ffefe2">
    							<td colspan="3"></td>
    							<td>{{{ $tkey }}}</td>
    							<td><?php echo number_format((float)$tvalues, 2) ?>  /-</td>
    						</tr>
                        @endforeach
						<tr bgcolor="#ffefe2">
							<td colspan="3"></td>
							<td>Total</td>
							<td><?php echo number_format((float)$allworkorderdata->grand_total, 2) ?> /-</td>
						</tr>

						<tr class="bg-org">
							<td colspan="3"></td>
							<td><b class="clr-wht">Total to be Paid</b></td>
							<td><b class="clr-wht"><?php echo number_format((float)$allworkorderdata->grand_total, 2) ?> /-</b></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="right">
					<p class="clr-org mar-top-20">Thanks for choosing Pride Webakruti Pvt. Ltd.</p>
				</td>
			</tr>

			<tr>
				<td>
					<p class="mar-top-30">
						<i><b class="clr-org">Payment:</b> We accept cash, online transfer money order check (payable to {{ $allworkorderdata->domains->name }}) Paypal and credit cards.</i>
					</p>
				</td>
			</tr>
			<tr>
				<td>
					<table class="mar-top-30" cellspacing="0" cellpadding="10" width="100%">
					    <tr style="background-color:#ededed;">
							<td>
								Bank Account Details
							</td>
							<td colspan="2">
								<b>Bank Name:</b> {{ $allworkorderdata->domains->bankname }}
							</td>
						</tr>
					    <tr style="background-color:#f7f7f7;">
					        <td>
								<b>Company:</b> {{ $allworkorderdata->domains->name }}
							</td>
							<td>
								<b>A/C No:</b> {{ $allworkorderdata->domains->bankaccno }}
							</td>
							<td>
								<b>IFSC Code:</b> {{ $allworkorderdata->domains->ifsc }}
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="100%" cellpadding="10" cellspacing="0" class="mar-top-30" style="font-size:12px;border-top:1px solid #eee; position:fixed;bottom:30">
						<tr>
						    <td>Website: {{ $allworkorderdata->domains->website }}</td>
							<td>Mob No: 91 {{ $allworkorderdata->domains->mobile }} / 91 {{ $allworkorderdata->domains->mobile2 }}</td>
							<td>Email Id: {{ $allworkorderdata->domains->email }}</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>